
//Class : ExceptionBusinessValidation
//Extends ExceptionMethod
//Author: Jason Vogel
//Date  : 03/30/2001
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionBusinessValidation extends ExceptionMethod.
 */

public class ExceptionBusinessValidation extends ExceptionMethod
{   
    // Default Constructor
    public ExceptionBusinessValidation() { }
    
    // Constructor
    public ExceptionBusinessValidation(String message) {
        super(message);
    }
}