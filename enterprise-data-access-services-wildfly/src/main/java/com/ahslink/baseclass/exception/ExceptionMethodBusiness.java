//Class : ExceptionMethodBusiness
//Extends ExceptionMethod
//Author: Mafaz Zafar
//Date  : 9/7/2000
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionMethodBusiness extends ExceptionMethod.
 */

public class ExceptionMethodBusiness extends ExceptionMethod
{   
  //Default Constructor
    public ExceptionMethodBusiness(){ }
    
  //Constructor
       
   public ExceptionMethodBusiness(String message)
     {
        super(message);
     }
}