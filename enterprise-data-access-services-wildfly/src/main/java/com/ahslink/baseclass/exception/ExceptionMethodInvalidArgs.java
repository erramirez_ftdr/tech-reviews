//Class : ExceptionMethodInvalidArgs
//Extends ExceptionMethod
//Author: Mafaz Zafar
//Date  : 9/7/2000
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionMethodInvalidArgs extends ExceptionMethod.
 */

public class ExceptionMethodInvalidArgs extends ExceptionMethod
{   
  //Default Constructor
    public ExceptionMethodInvalidArgs(){ }
    
  //Constructor
       
   public ExceptionMethodInvalidArgs(String message)
     {
        super(message);
     }
}