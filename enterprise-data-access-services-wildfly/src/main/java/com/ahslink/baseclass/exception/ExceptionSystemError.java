//Class : ExceptionMethodInvalidArgs
//Extends ExceptionMethod
//Author: Brian Johnston
//Date  : 04/03/2000
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionMethodInvalidArgs extends ExceptionMethod.
 */

public class ExceptionSystemError extends ExceptionMethod
{   
  //Default Constructor
    public ExceptionSystemError(){ }
    
  //Constructor
       
   public ExceptionSystemError(String message)
     {
        super(message);
     }
}