package com.ahslink.baseclass.exception;

/**
 * ExceptionInvalidPresentationSession extends Exception.
 */

public class ExceptionInvalidPresentationSession extends Exception
{   
  //Default Constructor
    public ExceptionInvalidPresentationSession(){ }
    
  //Constructor
       
   public ExceptionInvalidPresentationSession(String message)
     {
        super(message);
     }
}