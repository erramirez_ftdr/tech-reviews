package com.ahslink.ejb.session;

import com.ahslink.baseclass.exception.ExceptionMethodInvalidArgs;
import model.DispatchDaHome;
import model.WorkOrderDispatchSummaryDa;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.Stateless;
import java.rmi.RemoteException;

@Stateless
public class DispatchBean implements Dispatch {

    private static final Logger LOGGER = Logger.getLogger(DispatchBean.class);

    @EJB
    DispatchDaHome dispatchDaHome;

    public boolean isValid(long dispatchId) throws ExceptionMethodInvalidArgs, EJBException, RemoteException, FinderException {
        boolean             isValid = false;
        WorkOrderDispatchSummaryDa dispatchDa = null;

        //Check the passed arguments.
        if(dispatchId == 0) {
            throw new ExceptionMethodInvalidArgs("Invalid argument passed :isValid -> dispatchId " + dispatchId);
        }

        try {
            dispatchDa = dispatchDaHome.findByPrimaryKey(dispatchId);
            isValid = true;
        }
        catch (FinderException exception) {
            LOGGER.info("FinderException for dispatchId:" + dispatchId);
        }
        catch (Exception exception) {
            throw new EJBException(exception.toString());
        }

        return isValid;
    }
}
