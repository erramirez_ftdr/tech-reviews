/* Generated by Together */

package com.ahslink.ejb.entity;
import java.io.Serializable;


/**
 * @stereotype container
 */

public class ContractCoveragesDaValueObject implements Serializable {

	private long contractId;
	private long coverageId;
	private long groupId ;
	private long productVersionId;
	private int coverageQuantity;
	private float coverageCost ;
	private float coverageGroupCost ;
       private String customerType;


	public ContractCoveragesDaValueObject() {
	}

	public ContractCoveragesDaValueObject(long contractId,
                                          long coverageId,
                                          long groupId ,
                                          long productVersionId,
                                          int coverageQuantity,
                                          float coverageCost ,
                                          float coverageGroupCost,
                                          String customerType)
	                                       {
	   contractId = this.contractId;
	   coverageId = this.coverageId;
	   groupId  = this.groupId;
	   productVersionId = this.productVersionId;
	   coverageQuantity = this.coverageQuantity;
	   coverageCost  = this.coverageCost;
	   coverageGroupCost = this.coverageGroupCost;
           customerType = this.customerType;

	}

	public long getContractId(){
	    return this.contractId;
	}

	public void setContractId(long contractId){
	    this.contractId = contractId;
	}

    public long getCoverageId(){
        return this.coverageId;
    }


    public void setCoverageId(long coverageId){
        this.coverageId = coverageId;
    }

    public void setGroupId(long groupId){
        this.groupId = groupId;
    }

    public long getGroupId(){
        return this.groupId;
    }

    public void setProductVersionId(long productVersionId){
        this.productVersionId = productVersionId;
    }

    public long getProductVersionId(){
            return this.productVersionId;
    }

    public void setCoverageQuantity(int coverageQuantity){
        this.coverageQuantity = coverageQuantity;
    }

    public int getCoverageQuantity(){
        return this.coverageQuantity;
    }

    public void setCoverageCost(float coverageCost){
        this.coverageCost = coverageCost;
    }

    public float getCoverageCost(){
        return this.coverageCost;
    }

    public void setCoverageGroupCost(float coverageGroupCost){
        this.coverageGroupCost = coverageGroupCost;
    }

    public float getCoverageGroupCost(){
        return this.coverageGroupCost;
    }

    public String getCusomerType() {
      return this.customerType;
    }

    public void setCustomerType(String customerType ) {
      this.customerType = customerType;
    }



  }
