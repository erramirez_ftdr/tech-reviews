package com.ahslink.ejb.entity;

import java.io.Serializable;

public class ContactUsGroupsDaValueObject
  implements Serializable
{
  private long contactUsTopicGroupId;
  private String groupDescription;
  private String typeCode;
  private String typePrefix;
  
  public ContactUsGroupsDaValueObject(long contactUsTopicGroupId, String typeCode, String typePrefix, String groupDescription)
  {
    this.contactUsTopicGroupId = contactUsTopicGroupId;
    this.typeCode = typeCode;
    this.typePrefix = typePrefix;
    this.groupDescription = groupDescription;
  }
  
  public void setContactUsTopicGroupId(long contactUsTopicGroupId)
  {
    this.contactUsTopicGroupId = contactUsTopicGroupId;
  }
  
  public long getContactUsTopicGroupId()
  {
    return this.contactUsTopicGroupId;
  }
  
  public void setTypeCode(String typeCode)
  {
    this.typeCode = typeCode;
  }
  
  public String getTypeCode()
  {
    return this.typeCode;
  }
  
  public void setTypePrefix(String typePrefix)
  {
    this.typePrefix = typePrefix;
  }
  
  public String getTypePrefix()
  {
    return this.typePrefix;
  }
  
  public void setGroupDescription(String groupDescription)
  {
    this.groupDescription = groupDescription;
  }
  
  public String getGroupDescription()
  {
    return this.groupDescription;
  }
  
  public ContactUsGroupsDaValueObject() {}
}
