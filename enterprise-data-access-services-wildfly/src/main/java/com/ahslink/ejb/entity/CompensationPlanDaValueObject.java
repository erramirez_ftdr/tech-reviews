package com.ahslink.ejb.entity;

import java.io.Serializable;
import java.util.Date;

public class CompensationPlanDaValueObject
  implements Serializable
{
  public void setCommissionPlan(String commissionPlan)
  {
    this.commissionPlan = commissionPlan;
  }
  
  public String getCommissionPlan()
  {
    return this.commissionPlan;
  }
  
  public void setPlanName(String planName)
  {
    this.planName = planName;
  }
  
  public String getPlanName()
  {
    return this.planName;
  }
  
  public void setPlanTypeCode(String planTypeCode)
  {
    this.planTypeCode = planTypeCode;
  }
  
  public String getPlanTypeCode()
  {
    return this.planTypeCode;
  }
  
  public void setPlanTypePrefix(String planTypePrefix)
  {
    this.planTypePrefix = planTypePrefix;
  }
  
  public String getPlanTypePrefix()
  {
    return this.planTypePrefix;
  }
  
  public void setEffectiveDate2(Date effectiveDate2)
  {
    this.effectiveDate2 = effectiveDate2;
  }
  
  public Date getEffectiveDate2()
  {
    return this.effectiveDate2;
  }
  
  public void setExpirationDate2(Date expirationDate2)
  {
    this.expirationDate2 = expirationDate2;
  }
  
  public Date getExpirationDate2()
  {
    return this.expirationDate2;
  }
  
  public void setOkToNetIndicator(String okToNetIndicator)
  {
    this.okToNetIndicator = okToNetIndicator;
  }
  
  public String getOkToNetIndicator()
  {
    return this.okToNetIndicator;
  }
  
  public void setLastModified(Date lastModified)
  {
    this.lastModified = lastModified;
  }
  
  public Date getLastModified()
  {
    return this.lastModified;
  }
  
  public void setLastModifiedBy(String lastModifiedBy)
  {
    this.lastModifiedBy = lastModifiedBy;
  }
  
  public String getLastModifiedBy()
  {
    return this.lastModifiedBy;
  }
  
  protected String lastModifiedBy = "";
  protected Date expirationDate2;
  protected String okToNetIndicator = "";
  protected String planTypePrefix = "";
  protected String planName = "";
  protected String planTypeCode = "";
  protected String commissionPlan = "";
  protected Date effectiveDate2;
  protected Date lastModified;
}
