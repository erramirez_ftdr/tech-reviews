/* Generated by Together */

package com.ahslink.ejb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @stereotype container 
 */
public class DispatchContactDaValueObject implements Serializable {
    public long getDispatchId(){ return dispatchId; }

    public void setDispatchId(long dispatchId){ this.dispatchId = dispatchId; }

    public Date getEffective(){ return effective; }

    public void setEffective(Date effective){ this.effective = effective; }

    public Date getExpiration(){ return expiration; }

    public void setExpiration(Date expiration){ this.expiration = expiration; }

    public String getTitleCode(){ return titleCode; }

    public void setTitleCode(String titleCode){ this.titleCode = titleCode; }

    public String getTitleCodePrefix(){ return titleCodePrefix; }

    public void setTitleCodePrefix(String titleCodePrefix){ this.titleCodePrefix = titleCodePrefix; }

    public String getFirstName(){ return firstName; }

    public void setFirstName(String firstName){ this.firstName = firstName; }

    public char getMiddleInitial(){ return middleInitial; }

    public void setMiddleInitial(char middleInitial){ this.middleInitial = middleInitial; }

    public String getLastName(){ return lastName; }

    public void setLastName(String lastName){ this.lastName = lastName; }

    public String getPhoneCode(){ return phoneCode; }

    public void setPhoneCode(String phoneCode){ this.phoneCode = phoneCode; }

    public String getPhoneCodePrefix(){ return phoneCodePrefix; }

    public void setPhoneCodePrefix(String phoneCodePrefix){ this.phoneCodePrefix = phoneCodePrefix; }

    public String getPhoneNumber(){ return phoneNumber; }

    public void setPhoneNumber(String phoneNumber){ this.phoneNumber = phoneNumber; }

    public String getPhoneExtension(){ return phoneExtension; }

    public void setPhoneExtension(String phoneExtension){ this.phoneExtension = phoneExtension; }

    private long dispatchId;
    private Date effective;
    private Date expiration;
    private String titleCode;
    private String titleCodePrefix;
    private String firstName;
    private char middleInitial;
    private String lastName;
    private String phoneCode;
    private String phoneCodePrefix;
    private String phoneNumber;
    private String phoneExtension;
}
