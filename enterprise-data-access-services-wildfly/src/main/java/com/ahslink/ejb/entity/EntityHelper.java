package com.ahslink.ejb.entity;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EntityHelper {

	private static DataSource dataSource = null;
	private static final String DATASOURCE_JNDI_NAME = "java:jboss/datasources/oracle";

	public static Context getContext() throws NamingException{
		return new InitialContext();
	}

	@Deprecated
	public static Connection getDataSourceConnection(String dataSourceJndiName) throws NamingException, Exception{

		Context context = null;
		Connection connection = null;

		try{
			if(dataSource == null){				
				context = getContext();
				dataSource = (DataSource)context.lookup(DATASOURCE_JNDI_NAME);	

			}

			connection = dataSource.getConnection();

		} finally{
			if(context != null) {
				context.close();
			}
		}

		return connection;
	}

	public static Connection getDataSourceConnection() throws NamingException, Exception{

		Context context = null;
		Connection connection = null;

		try{
			if(dataSource == null){				
				context = getContext();
				dataSource = (DataSource)context.lookup(DATASOURCE_JNDI_NAME);	
			}

			connection = dataSource.getConnection();

		} finally{
			if(context != null) {
				context.close();
			}
		}

		return connection;
	}





	public static void cleanupDataSource(Connection connection, PreparedStatement preparedStatement) 
			throws SQLException, Exception{

		try{
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (SQLException exception){}

		try{
			if (connection != null) {
				connection.close();
			}
		} catch (Exception exception){}
	}
}
