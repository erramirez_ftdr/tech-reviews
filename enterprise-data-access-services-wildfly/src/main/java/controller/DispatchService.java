package controller;

import openuri.ObjectFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://www.openuri.org/", name = "DispatchServiceSoap")
@XmlSeeAlso({ObjectFactory.class})
public interface DispatchService {

    @WebMethod(operationName = "isValid")
    @RequestWrapper(localName = "isValid")
    @WebResult(name = "isValidResult")
    @ResponseWrapper(localName = "isValidResponse")
    public boolean isValid(
            @WebParam(name = "dispatchId")
                    long dispatchId
    );
}
