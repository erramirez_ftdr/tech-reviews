package controller;

import com.ahslink.ejb.session.Dispatch;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;

@WebService(serviceName = "DispatchService.jws",
        portName = "DispatchServiceSoap",
        name = "DispatchService",
        endpointInterface = "controller.DispatchService",
        targetNamespace = "http://www.openuri.org/")
public class DispatchServiceImpl implements DispatchService{

    private static final Logger LOGGER = Logger.getLogger(DispatchServiceImpl.class);

    private final String SERVICE_CONTROL_NAME = "DispatchServiceImpl";
    private final String GET_DISPATCH_STATUS_METHOD_NAME = "getDispatchStatus";
    private final String GET_DISPATCH_DETAIL_INFORMATION_METHOD_NAME = "getDispatchDetailInformation";
    private final String GET_DISPATCH_CONTACT_INFO_METHOD_NAME = "getDispatchContactInfo";
    private final String IS_CANCELLABLE_METHOD_NAME = "isCancellable";
    private final String SEND_DISPATCH_FORM_METHOD_NAME = "sendDispatchForm";
    private final String IS_VALID_METHOD_NAME = "isValid";
    private final String IS_DISPATCH_VALID_METHOD_NAME = "isDispatchValid";
    private final String GET_EXISTING_CASE_DATA_BY_DISPATCH = "getExistingCaseDataByDispatch";

    @EJB
    private Dispatch dispatchServiceManager;



    /**
     * @common:operation
     */
    public boolean isValid(long dispatchId)
    {
        Date externalProxyControlRequestSendTimeStamp = null;
        Date externalProxyControlRequestCompletedTimeStamp = null;
        long elapsedTime;
        double elapsedTimeSec;
        boolean verboseLogging = false;
        boolean dispatchIsValid = false;



        try {
            externalProxyControlRequestSendTimeStamp = new Date();
            dispatchIsValid = dispatchServiceManager.isValid(dispatchId);
        }
        catch (EJBException exception) {
            // Notify the application defect manager.
            //applicationDefectManager.processSystemError(exception);
        }
        catch (Exception exception) {
            // Notify the application defect manager.
            //applicationDefectManager.processSystemError(exception);
        }
        catch (Throwable throwableException) {
            // Notify the application defect manager.
            //applicationDefectManager.processSystemError( (Exception) throwableException);
        }

        // Capture the date and time the proxy service finished
        externalProxyControlRequestCompletedTimeStamp = new Date();

        // Calculate the External web service proxy elapsed time
        elapsedTime = (externalProxyControlRequestCompletedTimeStamp.getTime() - externalProxyControlRequestSendTimeStamp.getTime());
            elapsedTimeSec = (double)elapsedTime/1000;
            LOGGER.info("\"POST /" + SERVICE_CONTROL_NAME + "/" + IS_VALID_METHOD_NAME +"/?dispatchId="+dispatchId
                    + " HTTP/1.1\"" + " 200 0 ["+Double.toString(elapsedTimeSec)+"] ["+ Double.toString(elapsedTimeSec)+ "] " + "\"" + " \"" + " \" " + "\"" + " Response Time: "+Long.toString(elapsedTime) + "ms");


        return dispatchIsValid;
    }

}
