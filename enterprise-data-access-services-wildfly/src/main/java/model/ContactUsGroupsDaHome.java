package model;

import com.ahslink.config.AhsConstants;
import com.ahslink.ejb.entity.ContactUsGroupsDaValueObject;

import javax.ejb.FinderException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class ContactUsGroupsDaHome {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public ContactUsGroupsDaValueObject getValueObject(ContactUsGroupsDa contactUsGroupsDa) {
		ContactUsGroupsDaValueObject valueObject = new ContactUsGroupsDaValueObject();
		
		valueObject.setContactUsTopicGroupId(contactUsGroupsDa.getContactUsTopicGroupId());
		valueObject.setGroupDescription(contactUsGroupsDa.getGroupDescription());
		valueObject.setTypeCode(contactUsGroupsDa.getTypeCode());
		valueObject.setTypePrefix(contactUsGroupsDa.getTypePrefix());

		return valueObject;
	}

	public ContactUsGroupsDa findByPrimaryKey(long contactUsGroupId) throws FinderException {
		ContactUsGroupsDa contactUsGroupsDa = (ContactUsGroupsDa) em.createNamedQuery("ContactUsGroupsDa.findByPrimaryKey")
				.setParameter(1, contactUsGroupId)
				.getSingleResult();
		
		if(contactUsGroupsDa != null){
			return contactUsGroupsDa;
		}
		
		throw new FinderException("couldn't find;");
	}

	public Collection findByType(String typeCode) throws FinderException {
		Collection col = (Collection) em.createNamedQuery("ContactUsGroupsDa.findByType")
				.setParameter(1, typeCode)
				.getResultList();
		
		if(col != null && col.size() > 0){
			return col;
		}
		
		throw new FinderException("Couldn't find in findByType in ContactUsGroupsDaHome");
	}
}
