/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.ahslink.config.AhsConstants;
import com.ahslink.ejb.entity.EntityHelper;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

//import com.ahslink.ejb.session.SalesPersonBean;
//import com.ahslink.jpa.entity.ZipCodeDa;


@Stateless
public class ZipCodeStateCodeDao {
    private static final Logger LOGGER = Logger.getLogger(ZipCodeStateCodeDao.class);
          
    
    private static final String FIND_LATEST_LAST_MODIFIED_BY_STATE = "SELECT MAX(zcsc.last_modified) "
                                                                        + "FROM zip_code_state_code zcsc "
                                                                        + "WHERE zcsc.state = ?" ;
    
    @PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;
    
    /**
     * finds latest last_modified date associated with a state code
     * @param stateCode the code of the sate in question
     * @return a <code>Date</code> of zip codes
     */ 
    public Date findLatestLastModifiedByStateCode(String stateCode) throws Exception {

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        Date lastModified = null;
               
        try {            
             connection = EntityHelper.getDataSourceConnection();

             preparedStatement = connection.prepareStatement(FIND_LATEST_LAST_MODIFIED_BY_STATE);

             preparedStatement.setString(1, stateCode);
            
             resultSet = preparedStatement.executeQuery();
             
             while (resultSet.next()) {

                lastModified = resultSet.getDate(1);
               
             }
        } catch (Exception ex) {
        	
        	LOGGER.debug(ex.toString());
                throw ex;

        } finally {

            try {
                EntityHelper.cleanupDataSource(connection, preparedStatement);
            } catch (Exception ex) {

            }
        }        
        return lastModified;
    }
    
    public Date getLatestLastModifiedDateByState(String state) {
        return (Date) em.createNativeQuery(
                "SELECT MAX(zcsc.last_modified)" +
                        " FROM zip_code_state_code zcsc" +
                        " WHERE zcsc.state = ?1")
                .setParameter(1, state)
                .getSingleResult();
    }
    
}
