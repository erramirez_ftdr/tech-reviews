package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class CampaignReferenceColumnFrmtDao {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public Collection findCampaignReference(long campaignId, long partnerProgramId) {
		return (Collection) em.createNamedQuery("CampaignReferenceColumnFrmtDa.findCampaignReference")
				.setParameter(1, campaignId)
				.setParameter(2, partnerProgramId)
				.getResultList();
	}
}
