package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * The primary key class for the DISPATCH_CONTACT database table.
 * 
 */
@Embeddable
public class DispatchContactDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="DISPATCH_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long dispatchId;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE", unique=true, nullable=false)
	private Date effectiveDate;

	public DispatchContactDaPK() {
	}
	
	public DispatchContactDaPK(long dispatchId, Date effectiveDate) {
		this.dispatchId = dispatchId;
		this.effectiveDate = effectiveDate;
	}
	public long getDispatchId() {
		return this.dispatchId;
	}
	public void setDispatchId(long dispatchId) {
		this.dispatchId = dispatchId;
	}
	public Date getEffectiveDate() {
		return this.effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DispatchContactDaPK)) {
			return false;
		}
		DispatchContactDaPK castOther = (DispatchContactDaPK)other;
		return 
			(this.dispatchId == castOther.dispatchId)
			&& this.effectiveDate.equals(castOther.effectiveDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.dispatchId ^ (this.dispatchId >>> 32)));
		hash = hash * prime + this.effectiveDate.hashCode();
		
		return hash;
	}
}