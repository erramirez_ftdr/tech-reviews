package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the AHS_USER_AGENT database table.
 * 
 */
@Embeddable
public class AhsUserAgentDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="USER_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long userId;

	@Column(name="AGENT_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long agentId;

	@Column(name="AGENT_TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String agentTypeCode;

	@Column(name="AGENT_TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String agentTypePrefix;

	@Column(name="RELATIONSHIP_TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String relationshipTypeCode;

	@Column(name="RELATIONSHIP_TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String relationshipTypePrefix;

	public AhsUserAgentDaPK() {
	}
	public long getUserId() {
		return this.userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getAgentId() {
		return this.agentId;
	}
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}
	public String getAgentTypeCode() {
		return this.agentTypeCode;
	}
	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}
	public String getAgentTypePrefix() {
		return this.agentTypePrefix;
	}
	public void setAgentTypePrefix(String agentTypePrefix) {
		this.agentTypePrefix = agentTypePrefix;
	}
	public String getRelationshipTypeCode() {
		return this.relationshipTypeCode;
	}
	public void setRelationshipTypeCode(String relationshipTypeCode) {
		this.relationshipTypeCode = relationshipTypeCode;
	}
	public String getRelationshipTypePrefix() {
		return this.relationshipTypePrefix;
	}
	public void setRelationshipTypePrefix(String relationshipTypePrefix) {
		this.relationshipTypePrefix = relationshipTypePrefix;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AhsUserAgentDaPK)) {
			return false;
		}
		AhsUserAgentDaPK castOther = (AhsUserAgentDaPK)other;
		return 
			(this.userId == castOther.userId)
			&& (this.agentId == castOther.agentId)
			&& this.agentTypeCode.equals(castOther.agentTypeCode)
			&& this.agentTypePrefix.equals(castOther.agentTypePrefix)
			&& this.relationshipTypeCode.equals(castOther.relationshipTypeCode)
			&& this.relationshipTypePrefix.equals(castOther.relationshipTypePrefix);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.userId ^ (this.userId >>> 32)));
		hash = hash * prime + ((int) (this.agentId ^ (this.agentId >>> 32)));
		hash = hash * prime + this.agentTypeCode.hashCode();
		hash = hash * prime + this.agentTypePrefix.hashCode();
		hash = hash * prime + this.relationshipTypeCode.hashCode();
		hash = hash * prime + this.relationshipTypePrefix.hashCode();
		
		return hash;
	}
	@Override
	public String toString() {
		return "AhsUserAgentDaPK [userId=" + userId + ", agentId=" + agentId
				+ ", agentTypeCode=" + agentTypeCode + ", agentTypePrefix="
				+ agentTypePrefix + ", relationshipTypeCode="
				+ relationshipTypeCode + ", relationshipTypePrefix="
				+ relationshipTypePrefix + "]";
	}
}