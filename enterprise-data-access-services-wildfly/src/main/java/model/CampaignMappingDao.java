package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.Collection;

@Stateless
public class CampaignMappingDao {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public Collection findByCampaignId(long campaignId) {
		return (Collection) em.createNamedQuery("CampaignMappingDa.findByCampaignId")
				.setParameter(1, campaignId)
				.getResultList();
	}

	public Collection findByCampaignIdOrAccountIdOrEventIdOrContractChannel(long campaignId, long accountId, 
			long eventId, String paymentMethodTypeCode, String contactChannel) {

		return (Collection) em.createNamedQuery("CampaignMappingDa.findByCampaignIdOrAccountIdOrEventIdOrContractChannel")
				.setParameter(1, campaignId)
				.setParameter(2, accountId)
				.setParameter(3, new BigDecimal(eventId))
				.setParameter(4, paymentMethodTypeCode)
				.setParameter(5, contactChannel)
				.getResultList();
	}

//	public Collection findByCampaignOrAccountOrPayMethodOrEventOrContactChannel(long campaignId, long accountId, 
//			long eventId, String paymentMethodTypeCode, String contactChannel) {
//		
//		return (Collection) em.createNativeQuery(GET_SALES_SOURCE_BY_CAMPAIGN_OR_ACCOUNT_OR_PAYMETHOD_OR_EVENT_OR_CONTACT_CHANNEL_SQL)
//			.setParameter(1, campaignId)
//			.setParameter(2, accountId)
//			.setParameter(3, eventId)
//			.setParameter(4, paymentMethodTypeCode)
//			.setParameter(5, contactChannel)
//			.getResultList();
//	}
}
