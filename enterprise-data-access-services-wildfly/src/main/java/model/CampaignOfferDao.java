package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class CampaignOfferDao {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public Collection findByCampaignIdAndOfferTypeCode(long campaignId, String offerTypeCode) {

		if (offerTypeCode != null && offerTypeCode.trim().length() > 0 ) {
			return (Collection) em.createNamedQuery("CampaignOfferDa.findByCampaignId")
					.setParameter(1, campaignId)
					.getResultList();
		} else{
			return (Collection) em.createNamedQuery("CampaignOfferDa.findByCampaignIdAndOfferTypeCode")
					.setParameter(1, campaignId)
					.setParameter(2, offerTypeCode)
					.getResultList();			
		}	
	}

	public Collection findByOfferAndInclusionType(long offerId) {
		return em.createNamedQuery("CampaignOfferDa.findByOfferAndInclusionType")
				.setParameter(1, offerId)
				.getResultList();			
	}
}
