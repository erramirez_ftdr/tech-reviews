package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the DISPATCH_CONTACT database table.
 * 
 */
@Entity
@Table(name="DISPATCH_CONTACT")
@NamedQueries({
	@NamedQuery(name="DispatchContactDa.findAll", query="SELECT d FROM DispatchContactDa d"),
})
public class DispatchContactDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DispatchContactDaPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	@Column(length=10)
	private String extension;

	@Column(name="FIRST_NAME", length=20)
	private String firstName;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", length=31)
	private String lastModifiedBy;

	@Column(name="LAST_NAME", nullable=false, length=17)
	private String lastName;

	@Column(name="MIDDLE_INITIAL", length=1)
	private String middleInitial;

	@Column(name="PHONE_NUMBER", nullable=false, length=16)
	private String phoneNumber;

	@Column(name="PHONE_TYPE_CODE", nullable=false, length=10)
	private String phoneTypeCode;

	@Column(name="PHONE_TYPE_PREFIX", nullable=false, length=10)
	private String phoneTypePrefix;

	@Column(name="TITLE_CODE", nullable=false, length=10)
	private String titleCode;

	@Column(name="TITLE_PREFIX", nullable=false, length=10)
	private String titlePrefix;

	//bi-directional many-to-one association to WorkOrderDispatchSummaryDa
	@ManyToOne
	@JoinColumn(name="DISPATCH_ID", nullable=false, insertable=false, updatable=false)
	private WorkOrderDispatchSummaryDa workOrderDispatchSummary;

	public DispatchContactDa() {
	}

	public DispatchContactDaPK getId() {
		return this.id;
	}

	public void setId(DispatchContactDaPK id) {
		this.id = id;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleInitial() {
		return this.middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneTypeCode() {
		return this.phoneTypeCode;
	}

	public void setPhoneTypeCode(String phoneTypeCode) {
		this.phoneTypeCode = phoneTypeCode;
	}

	public String getPhoneTypePrefix() {
		return this.phoneTypePrefix;
	}

	public void setPhoneTypePrefix(String phoneTypePrefix) {
		this.phoneTypePrefix = phoneTypePrefix;
	}

	public String getTitleCode() {
		return this.titleCode;
	}

	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode;
	}

	public String getTitlePrefix() {
		return this.titlePrefix;
	}

	public void setTitlePrefix(String titlePrefix) {
		this.titlePrefix = titlePrefix;
	}

	public WorkOrderDispatchSummaryDa getWorkOrderDispatchSummary() {
		return this.workOrderDispatchSummary;
	}

	public void setWorkOrderDispatchSummary(WorkOrderDispatchSummaryDa workOrderDispatchSummary) {
		this.workOrderDispatchSummary = workOrderDispatchSummary;
	}

}