package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class CampaignEventPhoneDao {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public Collection findCampaignEventByPhoneLastFour(String campaignPhoneNumber, long campaignId) {
		return (Collection) em.createNamedQuery("CampaignEventPhoneDa.findCampaignEventByPhoneLastFour")
				.setParameter(1, campaignPhoneNumber)
				.setParameter(2, campaignId)
				.getResultList();
	}

	public Collection findCampaignEventByPhone(String phoneNumber, long campaignId) {
		return (Collection) em.createNamedQuery("CampaignEventPhoneDa.findCampaignEventByPhone")
				.setParameter(1, phoneNumber)
				.setParameter(2, campaignId)
				.getResultList();

	}
}
