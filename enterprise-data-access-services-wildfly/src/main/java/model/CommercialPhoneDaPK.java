package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the COMMERCIAL_PHONE database table.
 * 
 */
@Embeddable
public class CommercialPhoneDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COMMERCIAL_PROPERTY_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long commercialPropertyId;

	@Column(name="PHONE_NUMBER", unique=true, nullable=false, length=16)
	private String phoneNumber;

	@Column(name="TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String typeCode;

	@Column(name="TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String typePrefix;

	public CommercialPhoneDaPK() {
	}
	public long getCommercialPropertyId() {
		return this.commercialPropertyId;
	}
	public void setCommercialPropertyId(long commercialPropertyId) {
		this.commercialPropertyId = commercialPropertyId;
	}
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getTypeCode() {
		return this.typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTypePrefix() {
		return this.typePrefix;
	}
	public void setTypePrefix(String typePrefix) {
		this.typePrefix = typePrefix;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CommercialPhoneDaPK)) {
			return false;
		}
		CommercialPhoneDaPK castOther = (CommercialPhoneDaPK)other;
		return 
			(this.commercialPropertyId == castOther.commercialPropertyId)
			&& this.phoneNumber.equals(castOther.phoneNumber)
			&& this.typeCode.equals(castOther.typeCode)
			&& this.typePrefix.equals(castOther.typePrefix);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.commercialPropertyId ^ (this.commercialPropertyId >>> 32)));
		hash = hash * prime + this.phoneNumber.hashCode();
		hash = hash * prime + this.typeCode.hashCode();
		hash = hash * prime + this.typePrefix.hashCode();
		
		return hash;
	}
}