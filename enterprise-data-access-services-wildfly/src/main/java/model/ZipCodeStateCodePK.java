package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the ZIP_CODE_STATE_CODE database table.
 * 
 */
@Embeddable
public class ZipCodeStateCodePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ZIP_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String zipCode;

	@Column(name="\"STATE\"", insertable=false, updatable=false, unique=true, nullable=false, length=2)
	private String state;

	public ZipCodeStateCodePK() {
	}
	public String getZipCode() {
		return this.zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getState() {
		return this.state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ZipCodeStateCodePK)) {
			return false;
		}
		ZipCodeStateCodePK castOther = (ZipCodeStateCodePK)other;
		return 
			this.zipCode.equals(castOther.zipCode)
			&& this.state.equals(castOther.state);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.zipCode.hashCode();
		hash = hash * prime + this.state.hashCode();
		
		return hash;
	}
}