package model;

import com.ahslink.config.AhsConstants;
import com.ahslink.ejb.entity.EntityHelper;
import org.apache.log4j.Logger;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;

@Stateless
public class ApiHealthChecksDao {
	
	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;
	
	private static final Logger LOGGER = Logger.getLogger(ApiHealthChecksDao.class);
	
	private static final String CREATE_API_HEALTH_CHECK = "INSERT INTO api_health_checks "+
		      "(api_name, api_data,  health_check_timestamp) "+
		      "VALUES( ?, ?, ?) ";
	
	public boolean create(String apiName, String apiData)
					throws CreateException, FinderException {

		Connection          connection = null;
		PreparedStatement   preparedStatement = null;
		boolean result = false;

		try {
			
			LOGGER.debug("getting connecting and entering new request!");
			
			connection = EntityHelper.getDataSourceConnection();
			// SQL
			preparedStatement = connection.prepareStatement(CREATE_API_HEALTH_CHECK);
			preparedStatement.setString   (1, apiName);
			preparedStatement.setString   (2, apiData);
			preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));

			preparedStatement.executeQuery();
			
			result =  true;
		} catch (CreateException exception) {
			throw exception;
		} catch (Exception exception) {
			LOGGER.error("Unknown exception in create", exception);
			throw new CreateException(exception.toString());
		} finally {
			try {
				EntityHelper.cleanupDataSource(connection, preparedStatement);
			}catch (Exception exception) {
				throw new EJBException(exception.toString());
			}
		}
		
		return result;			
	}
}
