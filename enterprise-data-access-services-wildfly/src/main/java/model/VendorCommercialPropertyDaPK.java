package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the VENDOR_COMMERCIAL_PROPERTY database table.
 * 
 */
@Embeddable
public class VendorCommercialPropertyDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="VENDOR_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long vendorId;

	@Column(name="COMMERCIAL_PROPERTY_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long commercialPropertyId;

	public VendorCommercialPropertyDaPK() {
	}
	public long getVendorId() {
		return this.vendorId;
	}
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	public long getCommercialPropertyId() {
		return this.commercialPropertyId;
	}
	public void setCommercialPropertyId(long commercialPropertyId) {
		this.commercialPropertyId = commercialPropertyId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof VendorCommercialPropertyDaPK)) {
			return false;
		}
		VendorCommercialPropertyDaPK castOther = (VendorCommercialPropertyDaPK)other;
		return 
			(this.vendorId == castOther.vendorId)
			&& (this.commercialPropertyId == castOther.commercialPropertyId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.vendorId ^ (this.vendorId >>> 32)));
		hash = hash * prime + ((int) (this.commercialPropertyId ^ (this.commercialPropertyId >>> 32)));
		
		return hash;
	}
	@Override
	public String toString() {
		return "VendorCommercialPropertyDaPK [vendorId=" + vendorId
				+ ", commercialPropertyId=" + commercialPropertyId + "]";
	}
}