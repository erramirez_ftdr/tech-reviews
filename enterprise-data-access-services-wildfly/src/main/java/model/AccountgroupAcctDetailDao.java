package model;

import com.ahslink.ejb.entity.EntityHelper;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@Stateless
public class AccountgroupAcctDetailDao {

	private static final String GET_CONTRACT_EFFECTIVE_DATE = 
			"SELECT DETAIL_TYPE_PREFIX, DETAIL_TYPE_CODE, VALUE FROM ACCOUNTGROUP_ACCOUNT, ACCOUNTGROUP_ACCT_DETAIL "
					+ "WHERE ACCOUNTGROUP_ACCOUNT.ACCOUNT_ID = ? "
					+ "AND ACCOUNTGROUP_ACCOUNT.ACCOUNTGROUP_ACCOUNT_ID "
					+ "= ACCOUNTGROUP_ACCT_DETAIL.ACCOUNTGROUP_ACCOUNT_ID ";


	public Collection getContractEffectiveDateByAccountId(long accountId){

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		ArrayList<AccountgroupAcctDetailDa> collection = new ArrayList<AccountgroupAcctDetailDa>();

		try{
			connection = EntityHelper.getDataSourceConnection();

			preparedStatement = connection.prepareStatement(GET_CONTRACT_EFFECTIVE_DATE);
			preparedStatement.setLong(1, accountId);

			// Execute the query
			preparedStatement.executeQuery();

			// Find Entities in Database
			resultSet = preparedStatement.getResultSet();

			//Create a new value Object
			while(resultSet.next()){
				AccountgroupAcctDetailDa accountgroupAcctDetailDa = new AccountgroupAcctDetailDa();
				accountgroupAcctDetailDa.setDetailTypeCode(resultSet.getString(1));
				accountgroupAcctDetailDa.setDetailTypePrefix(resultSet.getString(2));
				accountgroupAcctDetailDa.setValue(resultSet.getString(3));

				collection.add(accountgroupAcctDetailDa);
			}

		} catch (SQLException e) {
			throw new EJBException(e);
		} catch (NamingException e) {
			throw new EJBException(e);
		} catch (Exception e) {
			throw new EJBException("Couldn't connect to db: "+e);
		} finally{
			try {
				EntityHelper.cleanupDataSource(connection, preparedStatement);
			}catch (Exception e) {}
		}

		return collection;
	}

}
