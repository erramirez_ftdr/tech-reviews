package model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the WORK_ORDER_DISPATCH_SUMMARY database table.
 * 
 */
@Entity
@Table(name="WORK_ORDER_DISPATCH_SUMMARY")
@NamedQueries({
	@NamedQuery(name="WorkOrderDispatchSummaryDa.findAll", query="SELECT w FROM WorkOrderDispatchSummaryDa w"),
	@NamedQuery(name="WorkOrderDispatchSummaryDa.findByPrimaryKey",
		query="SELECT w FROM WorkOrderDispatchSummaryDa w WHERE w.dispatchId = ?1"),
})
public class WorkOrderDispatchSummaryDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DISPATCH_ID", unique=true, nullable=false, precision=20)
	private long dispatchId;

	@Temporal(TemporalType.DATE)
	@Column(name="APPOINTMENT_TIMESTAMP")
	private Date appointmentTimestamp;

	@Column(name="AUTHORIZED_GRAND_TOTAL", precision=7, scale=2)
	private BigDecimal authorizedGrandTotal;

	@Column(name="AUTHORIZED_LABOR_TO_DATE", precision=7, scale=2)
	private BigDecimal authorizedLaborToDate;

	@Column(name="CHECKOUT_IND", nullable=false, length=1)
	private String checkoutInd;

	@Column(name="DISPATCH_DELAY_COUNTER", precision=3)
	private BigDecimal dispatchDelayCounter;

    // JEP 1/7/2016 TemporalType.DATE causes the time part of the timestamp
    // to be left off when retrieving records. changing this to
    // TemporalType.TIMESTAMP will keep the time component.
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DISPATCH_TIMESTAMP")
	private Date dispatchTimestamp;

	@Temporal(TemporalType.DATE)
	@Column(name="HOLD_UNTIL_TIMESTAMP")
	private Date holdUntilTimestamp;

	@Column(name="INVOICE_GROSS_AMOUNT", precision=10, scale=2)
	private BigDecimal invoiceGrossAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", length=31)
	private String lastModifiedBy;

	@Column(name="PREFERRED_VENDOR_IND", length=1)
	private String preferredVendorInd;

	@Temporal(TemporalType.DATE)
	@Column(name="SERVICE_DATE")
	private Date serviceDate;

	@Column(name="STATED_SVC_FEE_COLLECTED", precision=10, scale=2)
	private BigDecimal statedSvcFeeCollected;

	@Column(name="SVC_FEE_AMOUNT", precision=10, scale=2)
	private BigDecimal svcFeeAmount;

	@Column(name="SVC_FEE_COLLECTED", precision=10, scale=2)
	private BigDecimal svcFeeCollected;

	@Column(name="SVC_FEE_PAID_BY_NBR", length=20)
	private String svcFeePaidByNbr;

	@Column(name="TOTAL_WORK_TIME", length=10)
	private String totalWorkTime;

	@Column(name="WORK_DESCRIPTION", length=80)
	private String workDescription;

	@Column(name="WORK_ORDER_ID", precision=20)
	private BigDecimal workOrderId;

//	//bi-directional many-to-one association to ArInvoice
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<ArInvoice> arInvoices;

//	//bi-directional many-to-one association to Authorization
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<Authorization> authorizations;
//
//	//bi-directional many-to-one association to CilSummary
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<CilSummary> cilSummaries;

	//bi-directional many-to-one association to DispatchContactDa
	@OneToMany(mappedBy="workOrderDispatchSummary")
	private List<DispatchContactDa> dispatchContacts;

//	//bi-directional many-to-one association to FaxActivityDtl
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<FaxActivityDtl> faxActivityDtls;

//	//bi-directional many-to-one association to FollowUpQueue
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<FollowUpQueue> followUpQueues;
//
//	//bi-directional many-to-one association to PartRequest
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<PartRequest> partRequests;
//
//	//bi-directional many-to-one association to SecondOpinion
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<SecondOpinion> secondOpinions;
//
//	//bi-directional many-to-one association to SurveyInvitesSatisfaction
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<SurveyInvitesSatisfaction> surveyInvitesSatisfactions;
//
//	//bi-directional many-to-one association to VendorHwhInventory
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<VendorHwhInventory> vendorHwhInventories;

//	//bi-directional many-to-one association to VendorInvoice
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<VendorInvoiceDa> vendorInvoices;
//
//	//bi-directional many-to-one association to VendorTempInvoice
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<VendorTempInvoice> vendorTempInvoices;

//	//bi-directional many-to-one association to VqrActivityDtl
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<VqrActivityDtl> vqrActivityDtls;

	//bi-directional many-to-one association to AhsUserDa
//	@ManyToOne
//	@JoinColumn(name="DISPATCH_USER_ID")
//	private AhsUserDa ahsUser1;
//
//	//bi-directional many-to-one association to AhsUserDa
//	@ManyToOne
//	@JoinColumn(name="USER_ID", nullable=false)
//	private AhsUserDa ahsUser2;

//	//bi-directional many-to-one association to ContractPaymentType
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="SVC_FEE_PAID_BY_TYPE_CD", referencedColumnName="TYPE_CODE"),
//		@JoinColumn(name="SVC_FEE_PAID_BY_TYPE_PREFIX", referencedColumnName="TYPE_PREFIX")
//		})
//	private TypesDa type1;
//
//	//bi-directional many-to-one association to ContractPaymentType
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="SVC_FEE_SOURCE_CODE", referencedColumnName="TYPE_CODE"),
//		@JoinColumn(name="SVC_FEE_SOURCE_PREFIX", referencedColumnName="TYPE_PREFIX")
//		})
//	private TypesDa type2;
//
//	//bi-directional many-to-one association to ContractPaymentType
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="REASON_TYPE_CODE", referencedColumnName="TYPE_CODE"),
//		@JoinColumn(name="REASON_TYPE_PREFIX", referencedColumnName="TYPE_PREFIX")
//		})
//	private TypesDa type3;
//
//	//bi-directional many-to-one association to ContractPaymentType
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="DISPATCH_BY_TYPE_CD", referencedColumnName="TYPE_CODE"),
//		@JoinColumn(name="DISPATCH_BY_TYPE_PREFIX", referencedColumnName="TYPE_PREFIX")
//		})
//	private TypesDa type4;

	//bi-directional many-to-one association to ContractPaymentType
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="DISPATCH_TYPE_CD", referencedColumnName="TYPE_CODE"),
//		@JoinColumn(name="DISPATCH_TYPE_PREFIX", referencedColumnName="TYPE_PREFIX")
//		})
//	private TypesDa type5;

	//bi-directional many-to-one association to VendorCommercialPropertyDa
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="COMMERCIAL_PROPERTY_ID", referencedColumnName="COMMERCIAL_PROPERTY_ID"),
//		@JoinColumn(name="VENDOR_ID", referencedColumnName="VENDOR_ID")
//		})
//	private VendorCommercialPropertyDa vendorCommercialProperty;

	//bi-directional many-to-one association to WorkOrderDispatchSummaryDa
	@ManyToOne
	@JoinColumn(name="ORIGINATING_DISPATCH_ID")
	private WorkOrderDispatchSummaryDa workOrderDispatchSummary;

	//bi-directional many-to-one association to WorkOrderDispatchSummaryDa
	@OneToMany(mappedBy="workOrderDispatchSummary")
	private List<WorkOrderDispatchSummaryDa> workOrderDispatchSummaries;

//	//bi-directional many-to-one association to WoDispatchServiceFee
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<WoDispatchServiceFee> woDispatchServiceFees;

	//bi-directional many-to-one association to WoLineComponent
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<WoLineComponentDa> woLineComponents;
//
//	//bi-directional many-to-one association to WoLineCompAttr
//	@OneToMany(mappedBy="workOrderDispatchSummary")
//	private List<WoLineCompAttrDa> woLineCompAttrs;

	public WorkOrderDispatchSummaryDa() {
	}

	public long getDispatchId() {
		return this.dispatchId;
	}

	public void setDispatchId(long dispatchId) {
		this.dispatchId = dispatchId;
	}

	public Date getAppointmentTimestamp() {
		return this.appointmentTimestamp;
	}

	public void setAppointmentTimestamp(Date appointmentTimestamp) {
		this.appointmentTimestamp = appointmentTimestamp;
	}

	public BigDecimal getAuthorizedGrandTotal() {
		return this.authorizedGrandTotal;
	}

	public void setAuthorizedGrandTotal(BigDecimal authorizedGrandTotal) {
		this.authorizedGrandTotal = authorizedGrandTotal;
	}

	public BigDecimal getAuthorizedLaborToDate() {
		return this.authorizedLaborToDate;
	}

	public void setAuthorizedLaborToDate(BigDecimal authorizedLaborToDate) {
		this.authorizedLaborToDate = authorizedLaborToDate;
	}

	public String getCheckoutInd() {
		return this.checkoutInd;
	}

	public void setCheckoutInd(String checkoutInd) {
		this.checkoutInd = checkoutInd;
	}

	public BigDecimal getDispatchDelayCounter() {
		return this.dispatchDelayCounter;
	}

	public void setDispatchDelayCounter(BigDecimal dispatchDelayCounter) {
		this.dispatchDelayCounter = dispatchDelayCounter;
	}

	public Date getDispatchTimestamp() {
		return this.dispatchTimestamp;
	}

	public void setDispatchTimestamp(Date dispatchTimestamp) {
		this.dispatchTimestamp = dispatchTimestamp;
	}

	public Date getHoldUntilTimestamp() {
		return this.holdUntilTimestamp;
	}

	public void setHoldUntilTimestamp(Date holdUntilTimestamp) {
		this.holdUntilTimestamp = holdUntilTimestamp;
	}

	public BigDecimal getInvoiceGrossAmount() {
		return this.invoiceGrossAmount;
	}

	public void setInvoiceGrossAmount(BigDecimal invoiceGrossAmount) {
		this.invoiceGrossAmount = invoiceGrossAmount;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getPreferredVendorInd() {
		return this.preferredVendorInd;
	}

	public void setPreferredVendorInd(String preferredVendorInd) {
		this.preferredVendorInd = preferredVendorInd;
	}

	public Date getServiceDate() {
		return this.serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public BigDecimal getStatedSvcFeeCollected() {
		return this.statedSvcFeeCollected;
	}

	public void setStatedSvcFeeCollected(BigDecimal statedSvcFeeCollected) {
		this.statedSvcFeeCollected = statedSvcFeeCollected;
	}

	public BigDecimal getSvcFeeAmount() {
		return this.svcFeeAmount;
	}

	public void setSvcFeeAmount(BigDecimal svcFeeAmount) {
		this.svcFeeAmount = svcFeeAmount;
	}

	public BigDecimal getSvcFeeCollected() {
		return this.svcFeeCollected;
	}

	public void setSvcFeeCollected(BigDecimal svcFeeCollected) {
		this.svcFeeCollected = svcFeeCollected;
	}

	public String getSvcFeePaidByNbr() {
		return this.svcFeePaidByNbr;
	}

	public void setSvcFeePaidByNbr(String svcFeePaidByNbr) {
		this.svcFeePaidByNbr = svcFeePaidByNbr;
	}

	public String getTotalWorkTime() {
		return this.totalWorkTime;
	}

	public void setTotalWorkTime(String totalWorkTime) {
		this.totalWorkTime = totalWorkTime;
	}

	public String getWorkDescription() {
		return this.workDescription;
	}

	public void setWorkDescription(String workDescription) {
		this.workDescription = workDescription;
	}

	public BigDecimal getWorkOrderId() {
		return this.workOrderId;
	}

	public void setWorkOrderId(BigDecimal workOrderId) {
		this.workOrderId = workOrderId;
	}

//	public List<ArInvoice> getArInvoices() {
//		return this.arInvoices;
//	}
//
//	public void setArInvoices(List<ArInvoice> arInvoices) {
//		this.arInvoices = arInvoices;
//	}
//
//	public ArInvoice addArInvoice(ArInvoice arInvoice) {
//		getArInvoices().add(arInvoice);
//		arInvoice.setWorkOrderDispatchSummary(this);
//
//		return arInvoice;
//	}
//
//	public ArInvoice removeArInvoice(ArInvoice arInvoice) {
//		getArInvoices().remove(arInvoice);
//		arInvoice.setWorkOrderDispatchSummary(null);
//
//		return arInvoice;
//	}

//	public List<Authorization> getAuthorizations() {
//		return this.authorizations;
//	}
//
//	public void setAuthorizations(List<Authorization> authorizations) {
//		this.authorizations = authorizations;
//	}
//
//	public Authorization addAuthorization(Authorization authorization) {
//		getAuthorizations().add(authorization);
//		authorization.setWorkOrderDispatchSummary(this);
//
//		return authorization;
//	}
//
//	public Authorization removeAuthorization(Authorization authorization) {
//		getAuthorizations().remove(authorization);
//		authorization.setWorkOrderDispatchSummary(null);
//
//		return authorization;
//	}
//
//	public List<CilSummary> getCilSummaries() {
//		return this.cilSummaries;
//	}
//
//	public void setCilSummaries(List<CilSummary> cilSummaries) {
//		this.cilSummaries = cilSummaries;
//	}
//
//	public CilSummary addCilSummary(CilSummary cilSummary) {
//		getCilSummaries().add(cilSummary);
//		cilSummary.setWorkOrderDispatchSummary(this);
//
//		return cilSummary;
//	}
//
//	public CilSummary removeCilSummary(CilSummary cilSummary) {
//		getCilSummaries().remove(cilSummary);
//		cilSummary.setWorkOrderDispatchSummary(null);
//
//		return cilSummary;
//	}

	public List<DispatchContactDa> getDispatchContacts() {
		return this.dispatchContacts;
	}

	public void setDispatchContacts(List<DispatchContactDa> dispatchContacts) {
		this.dispatchContacts = dispatchContacts;
	}

	public DispatchContactDa addDispatchContact(DispatchContactDa dispatchContact) {
		getDispatchContacts().add(dispatchContact);
		dispatchContact.setWorkOrderDispatchSummary(this);

		return dispatchContact;
	}

	public DispatchContactDa removeDispatchContact(DispatchContactDa dispatchContact) {
		getDispatchContacts().remove(dispatchContact);
		dispatchContact.setWorkOrderDispatchSummary(null);

		return dispatchContact;
	}

//	public List<FaxActivityDtl> getFaxActivityDtls() {
//		return this.faxActivityDtls;
//	}
//
//	public void setFaxActivityDtls(List<FaxActivityDtl> faxActivityDtls) {
//		this.faxActivityDtls = faxActivityDtls;
//	}
//
//	public FaxActivityDtl addFaxActivityDtl(FaxActivityDtl faxActivityDtl) {
//		getFaxActivityDtls().add(faxActivityDtl);
//		faxActivityDtl.setWorkOrderDispatchSummary(this);
//
//		return faxActivityDtl;
//	}

//	public FaxActivityDtl removeFaxActivityDtl(FaxActivityDtl faxActivityDtl) {
//		getFaxActivityDtls().remove(faxActivityDtl);
//		faxActivityDtl.setWorkOrderDispatchSummary(null);
//
//		return faxActivityDtl;
//	}

//	public List<FollowUpQueue> getFollowUpQueues() {
//		return this.followUpQueues;
//	}
//
//	public void setFollowUpQueues(List<FollowUpQueue> followUpQueues) {
//		this.followUpQueues = followUpQueues;
//	}
//
//	public FollowUpQueue addFollowUpQueue(FollowUpQueue followUpQueue) {
//		getFollowUpQueues().add(followUpQueue);
//		followUpQueue.setWorkOrderDispatchSummary(this);
//
//		return followUpQueue;
//	}
//
//	public FollowUpQueue removeFollowUpQueue(FollowUpQueue followUpQueue) {
//		getFollowUpQueues().remove(followUpQueue);
//		followUpQueue.setWorkOrderDispatchSummary(null);
//
//		return followUpQueue;
//	}
//
//	public List<PartRequest> getPartRequests() {
//		return this.partRequests;
//	}
//
//	public void setPartRequests(List<PartRequest> partRequests) {
//		this.partRequests = partRequests;
//	}
//
//	public PartRequest addPartRequest(PartRequest partRequest) {
//		getPartRequests().add(partRequest);
//		partRequest.setWorkOrderDispatchSummary(this);
//
//		return partRequest;
//	}
//
//	public PartRequest removePartRequest(PartRequest partRequest) {
//		getPartRequests().remove(partRequest);
//		partRequest.setWorkOrderDispatchSummary(null);
//
//		return partRequest;
//	}
//
//	public List<SecondOpinion> getSecondOpinions() {
//		return this.secondOpinions;
//	}
//
//	public void setSecondOpinions(List<SecondOpinion> secondOpinions) {
//		this.secondOpinions = secondOpinions;
//	}
//
//	public SecondOpinion addSecondOpinion(SecondOpinion secondOpinion) {
//		getSecondOpinions().add(secondOpinion);
//		secondOpinion.setWorkOrderDispatchSummary(this);
//
//		return secondOpinion;
//	}
//
//	public SecondOpinion removeSecondOpinion(SecondOpinion secondOpinion) {
//		getSecondOpinions().remove(secondOpinion);
//		secondOpinion.setWorkOrderDispatchSummary(null);
//
//		return secondOpinion;
//	}
//
//	public List<SurveyInvitesSatisfaction> getSurveyInvitesSatisfactions() {
//		return this.surveyInvitesSatisfactions;
//	}
//
//	public void setSurveyInvitesSatisfactions(List<SurveyInvitesSatisfaction> surveyInvitesSatisfactions) {
//		this.surveyInvitesSatisfactions = surveyInvitesSatisfactions;
//	}
//
//	public SurveyInvitesSatisfaction addSurveyInvitesSatisfaction(SurveyInvitesSatisfaction surveyInvitesSatisfaction) {
//		getSurveyInvitesSatisfactions().add(surveyInvitesSatisfaction);
//		surveyInvitesSatisfaction.setWorkOrderDispatchSummary(this);
//
//		return surveyInvitesSatisfaction;
//	}
//
//	public SurveyInvitesSatisfaction removeSurveyInvitesSatisfaction(SurveyInvitesSatisfaction surveyInvitesSatisfaction) {
//		getSurveyInvitesSatisfactions().remove(surveyInvitesSatisfaction);
//		surveyInvitesSatisfaction.setWorkOrderDispatchSummary(null);
//
//		return surveyInvitesSatisfaction;
//	}
//
//	public List<VendorHwhInventory> getVendorHwhInventories() {
//		return this.vendorHwhInventories;
//	}
//
//	public void setVendorHwhInventories(List<VendorHwhInventory> vendorHwhInventories) {
//		this.vendorHwhInventories = vendorHwhInventories;
//	}
//
//	public VendorHwhInventory addVendorHwhInventory(VendorHwhInventory vendorHwhInventory) {
//		getVendorHwhInventories().add(vendorHwhInventory);
//		vendorHwhInventory.setWorkOrderDispatchSummary(this);
//
//		return vendorHwhInventory;
//	}
//
//	public VendorHwhInventory removeVendorHwhInventory(VendorHwhInventory vendorHwhInventory) {
//		getVendorHwhInventories().remove(vendorHwhInventory);
//		vendorHwhInventory.setWorkOrderDispatchSummary(null);
//
//		return vendorHwhInventory;
//	}

//	public List<VendorInvoiceDa> getVendorInvoices() {
//		return this.vendorInvoices;
//	}

//	public void setVendorInvoices(List<VendorInvoiceDa> vendorInvoices) {
//		this.vendorInvoices = vendorInvoices;
//	}
//
//	public VendorInvoiceDa addVendorInvoice(VendorInvoiceDa vendorInvoice) {
//		getVendorInvoices().add(vendorInvoice);
//		vendorInvoice.setWorkOrderDispatchSummary(this);
//
//		return vendorInvoice;
//	}

//	public VendorInvoiceDa removeVendorInvoice(VendorInvoiceDa vendorInvoice) {
//		getVendorInvoices().remove(vendorInvoice);
//		vendorInvoice.setWorkOrderDispatchSummary(null);
//
//		return vendorInvoice;
//	}
//
//	public List<VendorTempInvoice> getVendorTempInvoices() {
//		return this.vendorTempInvoices;
//	}
//
//	public void setVendorTempInvoices(List<VendorTempInvoice> vendorTempInvoices) {
//		this.vendorTempInvoices = vendorTempInvoices;
//	}
//
//	public VendorTempInvoice addVendorTempInvoice(VendorTempInvoice vendorTempInvoice) {
//		getVendorTempInvoices().add(vendorTempInvoice);
//		vendorTempInvoice.setWorkOrderDispatchSummary(this);
//
//		return vendorTempInvoice;
//	}
//
//	public VendorTempInvoice removeVendorTempInvoice(VendorTempInvoice vendorTempInvoice) {
//		getVendorTempInvoices().remove(vendorTempInvoice);
//		vendorTempInvoice.setWorkOrderDispatchSummary(null);
//
//		return vendorTempInvoice;
//	}

//	public List<VqrActivityDtl> getVqrActivityDtls() {
//		return this.vqrActivityDtls;
//	}
//
//	public void setVqrActivityDtls(List<VqrActivityDtl> vqrActivityDtls) {
//		this.vqrActivityDtls = vqrActivityDtls;
//	}
//
//	public VqrActivityDtl addVqrActivityDtl(VqrActivityDtl vqrActivityDtl) {
//		getVqrActivityDtls().add(vqrActivityDtl);
//		vqrActivityDtl.setWorkOrderDispatchSummary(this);
//
//		return vqrActivityDtl;
//	}
//
//	public VqrActivityDtl removeVqrActivityDtl(VqrActivityDtl vqrActivityDtl) {
//		getVqrActivityDtls().remove(vqrActivityDtl);
//		vqrActivityDtl.setWorkOrderDispatchSummary(null);
//
//		return vqrActivityDtl;
//	}

//	public AhsUserDa getAhsUser1() {
//		return this.ahsUser1;
//	}
//
//	public void setAhsUser1(AhsUserDa ahsUser1) {
//		this.ahsUser1 = ahsUser1;
//	}
//
//	public AhsUserDa getAhsUser2() {
//		return this.ahsUser2;
//	}
//
//	public void setAhsUser2(AhsUserDa ahsUser2) {
//		this.ahsUser2 = ahsUser2;
//	}

//	public TypesDa getType1() {
//		return this.type1;
//	}
//
//	public void setType1(TypesDa type1) {
//		this.type1 = type1;
//	}
//
//	public TypesDa getType2() {
//		return this.type2;
//	}
//
//	public void setType2(TypesDa type2) {
//		this.type2 = type2;
//	}
//
//	public TypesDa getType3() {
//		return this.type3;
//	}
//
//	public void setType3(TypesDa type3) {
//		this.type3 = type3;
//	}
//
//	public TypesDa getType4() {
//		return this.type4;
//	}
//
//	public void setType4(TypesDa type4) {
//		this.type4 = type4;
//	}
//
//	public TypesDa getType5() {
//		return this.type5;
//	}
//
//	public void setType5(TypesDa type5) {
//		this.type5 = type5;
//	}

//	public VendorCommercialPropertyDa getVendorCommercialProperty() {
//		return this.vendorCommercialProperty;
//	}
//
//	public void setVendorCommercialProperty(VendorCommercialPropertyDa vendorCommercialProperty) {
//		this.vendorCommercialProperty = vendorCommercialProperty;
//	}

	public WorkOrderDispatchSummaryDa getWorkOrderDispatchSummary() {
		return this.workOrderDispatchSummary;
	}

	public void setWorkOrderDispatchSummary(WorkOrderDispatchSummaryDa workOrderDispatchSummary) {
		this.workOrderDispatchSummary = workOrderDispatchSummary;
	}

	public List<WorkOrderDispatchSummaryDa> getWorkOrderDispatchSummaries() {
		return this.workOrderDispatchSummaries;
	}

	public void setWorkOrderDispatchSummaries(List<WorkOrderDispatchSummaryDa> workOrderDispatchSummaries) {
		this.workOrderDispatchSummaries = workOrderDispatchSummaries;
	}

	public WorkOrderDispatchSummaryDa addWorkOrderDispatchSummary(WorkOrderDispatchSummaryDa workOrderDispatchSummary) {
		getWorkOrderDispatchSummaries().add(workOrderDispatchSummary);
		workOrderDispatchSummary.setWorkOrderDispatchSummary(this);

		return workOrderDispatchSummary;
	}

	public WorkOrderDispatchSummaryDa removeWorkOrderDispatchSummary(WorkOrderDispatchSummaryDa workOrderDispatchSummary) {
		getWorkOrderDispatchSummaries().remove(workOrderDispatchSummary);
		workOrderDispatchSummary.setWorkOrderDispatchSummary(null);

		return workOrderDispatchSummary;
	}

//	public List<WoDispatchServiceFee> getWoDispatchServiceFees() {
//		return this.woDispatchServiceFees;
//	}
//
//	public void setWoDispatchServiceFees(List<WoDispatchServiceFee> woDispatchServiceFees) {
//		this.woDispatchServiceFees = woDispatchServiceFees;
//	}
//
//	public WoDispatchServiceFee addWoDispatchServiceFee(WoDispatchServiceFee woDispatchServiceFee) {
//		getWoDispatchServiceFees().add(woDispatchServiceFee);
//		woDispatchServiceFee.setWorkOrderDispatchSummary(this);
//
//		return woDispatchServiceFee;
//	}
//
//	public WoDispatchServiceFee removeWoDispatchServiceFee(WoDispatchServiceFee woDispatchServiceFee) {
//		getWoDispatchServiceFees().remove(woDispatchServiceFee);
//		woDispatchServiceFee.setWorkOrderDispatchSummary(null);
//
//		return woDispatchServiceFee;
//	}

//	public List<WoLineComponentDa> getWoLineComponents() {
//		return this.woLineComponents;
//	}
//
//	public void setWoLineComponents(List<WoLineComponentDa> woLineComponents) {
//		this.woLineComponents = woLineComponents;
//	}
//
//	public WoLineComponentDa addWoLineComponent(WoLineComponentDa woLineComponent) {
//		getWoLineComponents().add(woLineComponent);
//		woLineComponent.setWorkOrderDispatchSummary(this);
//
//		return woLineComponent;
//	}
//
//	public WoLineComponentDa removeWoLineComponent(WoLineComponentDa woLineComponent) {
//		getWoLineComponents().remove(woLineComponent);
//		woLineComponent.setWorkOrderDispatchSummary(null);
//
//		return woLineComponent;
//	}
//
//	public List<WoLineCompAttrDa> getWoLineCompAttrs() {
//		return this.woLineCompAttrs;
//	}
//
//	public void setWoLineCompAttrs(List<WoLineCompAttrDa> woLineCompAttrs) {
//		this.woLineCompAttrs = woLineCompAttrs;
//	}
//
//	public WoLineCompAttrDa addWoLineCompAttr(WoLineCompAttrDa woLineCompAttr) {
//		getWoLineCompAttrs().add(woLineCompAttr);
//		woLineCompAttr.setWorkOrderDispatchSummary(this);
//
//		return woLineCompAttr;
//	}
//
//	public WoLineCompAttrDa removeWoLineCompAttr(WoLineCompAttrDa woLineCompAttr) {
//		getWoLineCompAttrs().remove(woLineCompAttr);
//		woLineCompAttr.setWorkOrderDispatchSummary(null);
//
//		return woLineCompAttr;
//	}

}