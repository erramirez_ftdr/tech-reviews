package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class CampaignEventDao {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public Collection findByCampaignIdAndCreativeCode(long campaignId, String creativeCode) {
		
		return (Collection) em.createNamedQuery("CampaignEventDa.findByCampaignIdAndCreativeCode")
				.setParameter(2, creativeCode)
				.setParameter(1, campaignId)
				.getResultList();
	}
}
