
quarkus:
  datasource:
    url: jdbc:oracle:thin:@AHSStarTest.testahs.com:1528:ORCLT06
    driver: oracle.jdbc.OracleDriver
    username: JAVA_BUSINESS_TIER
    password: thursday
    min-size: 3
    max-size: 13
  hibernate-orm:
    dialect: org.hibernate.dialect.Oracle10gDialect
    database:
        generation: none
    log:
      sql: false
  http:
    http2: true
    access-log:
      enabled: true
  cxf:
    path: /EnterpriseDataAccessServices/internalWebServices
    endpoint: /DispatchService.jws
    implementor: "com.ahslink.controller.DispatchServiceImpl"

