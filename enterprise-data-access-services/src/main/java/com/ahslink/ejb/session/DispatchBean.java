package com.ahslink.ejb.session;

import com.ahslink.baseclass.exception.ExceptionMethodInvalidArgs;
import model.DispatchDaHome;
import model.WorkOrderDispatchSummaryDa;
import org.jboss.logging.Logger;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.rmi.RemoteException;

@ApplicationScoped
public class DispatchBean implements Dispatch{

    private static final Logger LOGGER = Logger.getLogger(DispatchBean.class);

    @Inject
    DispatchDaHome dispatchDaHome;

    @Override
    public boolean isValid(long dispatchId) throws ExceptionMethodInvalidArgs, EJBException, RemoteException, FinderException {
        boolean             isValid = false;
        WorkOrderDispatchSummaryDa dispatchDa = null;

        //Check the passed arguments.
        if(dispatchId == 0) {
            throw new ExceptionMethodInvalidArgs("Invalid argument passed :isValid -> dispatchId " + dispatchId);
        }

        try {
            dispatchDa = dispatchDaHome.findByPrimaryKey(dispatchId);
            isValid = true;
        }
        catch (FinderException exception) {
            LOGGER.info("FinderException for dispatchId:" + dispatchId);
        }
        catch (Exception exception) {
            throw new EJBException(exception);
        }

        return isValid;
    }
}
