package com.ahslink.ejb.session;

import com.ahslink.baseclass.exception.ExceptionMethodInvalidArgs;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import java.rmi.RemoteException;

public interface Dispatch
{
	public static final String EMERGENCY_PRIORITY = "EMERGENCY";
	public static final String IN_QUEUED_DISPATCH_BY_TYPE_CODE = "QUEUED";
	public static final String NORMAL_PRIORITY = "NORMAL";
	public static final String QUEUED_DIRECT_DISPATCH_BY_TYPE_CODE = "QUEUED2";
	public static final String SUPER_HOLD_DISPATCH_BY_TYPE_CODE = "SUPERHOLD";

	public abstract boolean isValid(long paramLong)
			throws ExceptionMethodInvalidArgs, EJBException, RemoteException, FinderException;
}
