package com.ahslink.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class DispatchStatusDaValueObject
  implements Serializable
{
  private String applicationCode;
  private String applicationPrefix;
  private long dispatchId;
  private String statusCode;
  private String statusPrefix;
  private Timestamp statusTimestamp;
  
  public long getDispatchId()
  {
    return this.dispatchId;
  }
  
  public void setDispatchId(long dispatchId)
  {
    this.dispatchId = dispatchId;
  }
  
  public String getStatusCode()
  {
    return this.statusCode;
  }
  
  public void setStatusCode(String statusCode)
  {
    this.statusCode = statusCode;
  }
  
  public String getStatusPrefix()
  {
    return this.statusPrefix;
  }
  
  public void setStatusPrefix(String statusPrefix)
  {
    this.statusPrefix = statusPrefix;
  }
  
  public Timestamp getStatusTimestamp()
  {
    return this.statusTimestamp;
  }
  
  public void setStatusTimestamp(Timestamp statusTimestamp)
  {
    this.statusTimestamp = statusTimestamp;
  }
  
  public String getApplicationCode()
  {
    return this.applicationCode;
  }
  
  public void setApplicationCode(String applicationCode)
  {
    this.applicationCode = applicationCode;
  }
  
  public String getApplicationPrefix()
  {
    return this.applicationPrefix;
  }
  
  public void setApplicationPrefix(String applicationPrefix)
  {
    this.applicationPrefix = applicationPrefix;
  }
}
