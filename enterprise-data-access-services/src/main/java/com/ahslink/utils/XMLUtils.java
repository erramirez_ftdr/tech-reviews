package com.ahslink.utils;

import org.jboss.logging.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

public class XMLUtils {

	private static Logger LOGGER = Logger.getLogger(XMLUtils.class);

	//This map is used to convert full type names to their TYPE_CODE value as represented in the db
	private static final Map<String, String> typeConversionMap;
	static {
		Map<String, String> aMap  = new HashMap<String, String>();

		//sales and product sales channel values
		aMap.put("RealEstate", "RE");
		aMap.put("Renewal", "RN");
		aMap.put("DirectToConsumer", "DC");

		//warranty originator types
		aMap.put("InitiatingRealEstateOffice", "RE");
		aMap.put("SalesSource", "DS"); // TODO is this the correct value??
		aMap.put("ClosingCompany", "CC");
		
		//listing types
		aMap.put("BuyerOnly", "BUYER");
		aMap.put("BuyerSeller", "SELLER");

		typeConversionMap = Collections.unmodifiableMap(aMap);
	}

	/**
	 * Converts java.util.Date to XMLGregorianCalendar
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException 
	 */
	public static XMLGregorianCalendar dateToXMLGregorianCal(Date date){
		XMLGregorianCalendar xmlCal = null;

		if(date != null){
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			try {
				xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				LOGGER.error("Could not get instance of XMLGregorianCalendar!", e);
			}
		}

		return xmlCal;
	}

	/**
	 * Converts XMLGregorianCalendar to java.util.Date
	 * @param calendar
	 * @return
	 */
	public static Date xmlGregorianCalendartoDate(XMLGregorianCalendar calendar){
		if(calendar == null) {
			return null;
		}

		return calendar.toGregorianCalendar().getTime();
	}
	
	public static String convertType(String typeToConvert){
		return typeConversionMap.get(typeToConvert);
	}
}
