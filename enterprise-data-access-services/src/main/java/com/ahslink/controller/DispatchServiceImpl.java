package com.ahslink.controller;

import com.ahslink.ejb.session.Dispatch;
import org.jboss.logging.Logger;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;

@WebService(
        serviceName = "DispatchService.jws",
        portName = "DispatchServiceSoap",
        name = "DispatchService",
        endpointInterface = "com.ahslink.controller.DispatchService",
        targetNamespace = "http://www.openuri.org/")
public class DispatchServiceImpl implements DispatchService{

    private static final Logger LOGGER = Logger.getLogger(DispatchServiceImpl.class);

    private final String SERVICE_CONTROL_NAME = "DispatchServiceImpl";
    private final String IS_VALID_METHOD_NAME = "isValid";

    @Inject
    private Dispatch dispatchServiceManager;


    @Override
    public boolean isValid(long dispatchId)
    {
        boolean dispatchIsValid = false;
        Date externalProxyControlRequestSendTimeStamp = new Date();
        try {
            dispatchIsValid = dispatchServiceManager.isValid(dispatchId);
        }
        catch (EJBException exception) {
            LOGGER.error("EJBException ERROR:", exception);
        }
        catch (Exception exception) {
            LOGGER.error("Exception ERROR:", exception);
        }

        Date externalProxyControlRequestCompletedTimeStamp = new Date();
        long elapsedTime = (externalProxyControlRequestCompletedTimeStamp.getTime() - externalProxyControlRequestSendTimeStamp.getTime());
        double elapsedTimeSec = (double)elapsedTime/1000;
            LOGGER.info("\"POST /" + SERVICE_CONTROL_NAME + "/" + IS_VALID_METHOD_NAME +"/?dispatchId="+dispatchId
                    + " HTTP/1.1\"" + " 200 0 ["+Double.toString(elapsedTimeSec)+"] ["+ Double.toString(elapsedTimeSec)+ "] " + "\"" + " \"" + " \" " + "\"" + " Response Time: "+Long.toString(elapsedTime) + "ms");
        return dispatchIsValid;
    }

}
