package com.ahslink.controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://www.openuri.org/", name = "DispatchServiceSoap")
public interface DispatchService {

    @WebResult(name = "isValidResult", targetNamespace = "http://www.openuri.org/")
    @RequestWrapper(localName = "isValid", targetNamespace = "http://www.openuri.org/", className = "org.openuri.IsValid")
    @WebMethod(action = "http://www.openuri.org/isValid")
    @ResponseWrapper(localName = "isValidResponse", targetNamespace = "http://www.openuri.org/", className = "org.openuri.IsValidResponse")
    public boolean isValid(
            @WebParam(name = "dispatchId", targetNamespace = "http://www.openuri.org/")
                    long dispatchId
    );
}
