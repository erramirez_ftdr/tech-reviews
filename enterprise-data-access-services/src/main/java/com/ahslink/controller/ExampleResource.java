package com.ahslink.controller;

import com.ahslink.ejb.session.Dispatch;
import org.jboss.logging.Logger;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Date;

@Path("/example")
public class ExampleResource {

    private static final Logger LOGGER = Logger.getLogger(DispatchServiceImpl.class);

    private final String SERVICE_CONTROL_NAME = "DispatchServiceImpl";
    private final String IS_VALID_METHOD_NAME = "isValid";

    @Inject
    private Dispatch dispatchServiceManager;

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }


    @GET
    @Path("/isValid")
    @Produces(MediaType.TEXT_PLAIN)
    public boolean isValid(@QueryParam("dispatchId") long dispatchId) {
        boolean dispatchIsValid = false;
        Date externalProxyControlRequestSendTimeStamp = new Date();
        try {
            dispatchIsValid = dispatchServiceManager.isValid(dispatchId);
        }
        catch (EJBException exception) {
        }
        catch (Exception exception) {
            LOGGER.error("Error", exception);
        }
        catch (Throwable throwableException) {
        }

        Date externalProxyControlRequestCompletedTimeStamp = new Date();
        long elapsedTime = (externalProxyControlRequestCompletedTimeStamp.getTime() - externalProxyControlRequestSendTimeStamp.getTime());
        double elapsedTimeSec = (double)elapsedTime/1000;
        LOGGER.info("\"POST /" + SERVICE_CONTROL_NAME + "/" + IS_VALID_METHOD_NAME +"/?dispatchId="+dispatchId
                + " HTTP/1.1\"" + " 200 0 ["+Double.toString(elapsedTimeSec)+"] ["+ Double.toString(elapsedTimeSec)+ "] " + "\"" + " \"" + " \" " + "\"" + " Response Time: "+Long.toString(elapsedTime) + "ms");

        return dispatchIsValid;

    }
}
