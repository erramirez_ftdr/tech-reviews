package com.ahslink.config;

public class AhsConstants {
	public static final String AGENT_TYPE_PREFIX = "OFFICE";
	
	public static final String ENTITY_MANAGER_UNIT_NAME = "common-components";
	
	public static final String WORK_ORDER_DATASOURCE = "MEMPHISDATASOURCE";
	
	public static final String ACTIVITY_TYPE_PREFIX = "SF_WOACT";
	public static final String STATUS_TYPE_PREFIX = "SF_WOASTAT";
	public static final String REASON_TYPE_PREFIX = "SF_CVREA";
	
	public static final String AGENT_PHONES_TYPE_PREFIX_PHONE = "PHONE";
	public static final String DISTRIBUTOR_TYPE_PREFIX_OFFICE = "OFFICE";
	
	//Web Constants
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String BASIC_AUTHENTICATION = "Basic";
	public static final String LDAP_SECURITY_DOMAIN = "ahsLDAP";
	public static final String WWW_AUTHENTICATE_HEADER = "WWW-Authenticate";
	public static final String USER_SESSION_ATTRIBUTE_NAME = "user";
	
	//Error code messages
	public static final String HTTP_401_ERROR_MESSAGE = "Authorization required";
	public static final String HTTP_403_ERROR_MESSAGE = "Authentication Error";
}
