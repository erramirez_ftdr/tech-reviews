//Class : ExceptionMethod
//Extends Exception
//Author: Mafaz Zafar
//Date  : 9/7/2000
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionMethod extends Exception.
 */

public class ExceptionMethod extends Exception
{   
  //Default Constructor
    public ExceptionMethod(){ }
    
  //Constructor
       
   public ExceptionMethod(String message)
     {
        super(message);
     }
}