//Class : ExceptionprocessingCanceled
//Extends ExceptionMethod
//Author: Kelli Hewitt
//Date  : 9/27/2000
//Initial Version

package com.ahslink.baseclass.exception;

public class ExceptionProcessingCanceled extends ExceptionMethod {
    
    //Default Constructor
    public ExceptionProcessingCanceled() {  
    }
    
    // Constructor
    public ExceptionProcessingCanceled(String message) {
        super(message);   
    }
}
