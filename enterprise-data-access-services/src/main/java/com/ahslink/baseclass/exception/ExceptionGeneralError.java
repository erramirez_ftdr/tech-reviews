//Class : ExceptionMethodInvalidArgs
//Extends ExceptionMethod
//Author: Brian Johnston
//Date  : 04/03/2000
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionMethodInvalidArgs extends ExceptionMethod.
 */

public class ExceptionGeneralError extends ExceptionMethod
{   
  //Default Constructor
    public ExceptionGeneralError(){ }
    
  //Constructor
       
   public ExceptionGeneralError(String message)
     {
        super(message);
     }
}