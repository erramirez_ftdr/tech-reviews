
//Class : ExceptionPageValidation
//Extends ExceptionMethod
//Author: Jason Vogel
//Date  : 03/30/2001
//Initial Version

package com.ahslink.baseclass.exception;

/**
 * ExceptionPageValidation extends ExceptionMethod.
 */

public class ExceptionPageValidation extends ExceptionMethod
{   
    // Default Constructor
    public ExceptionPageValidation() { }
    
    // Constructor
    public ExceptionPageValidation(String message) {
        super(message);
    }
}