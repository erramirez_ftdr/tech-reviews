package com.ahslink.baseclass.exception;

/**
 * ExceptionInvalidPresentationSession extends Exception.
 */

public class ExceptionProcessingInvalid extends Exception
{   
  //Default Constructor
    public ExceptionProcessingInvalid(){ }
    
  //Constructor
       
   public ExceptionProcessingInvalid(String message)
     {
        super(message);
     }
}