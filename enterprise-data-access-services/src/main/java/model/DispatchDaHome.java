package model;

import org.hibernate.annotations.QueryHints;
import org.jboss.logging.Logger;

import javax.ejb.FinderException;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class DispatchDaHome {
	
	private static final Logger LOGGER = Logger.getLogger(DispatchDaHome.class);

    @PersistenceContext
    EntityManager em;
	
	private static final String FIND_LAST_KNOWN_BY_CONTRACT_SQL = 
		    "SELECT wods.dispatch_id" +
		    " FROM work_order wo, work_order_dispatch_summary wods, dispatch_activity_dtl dtl" +
	        " WHERE wo.work_order_id = wods.work_order_id" +
	        " AND wods.dispatch_id = dtl.dispatch_id" +
	        " AND dtl.status_type_code != 'CANCEL'" +
	        " AND wo.work_order_type_code = 'NORMAL'" +
	        " AND wo.work_order_type_prefix = 'SF_WOTYP'" +
	        " AND wo.work_order_status_type_code <> 'CANCEL'" +
	        " AND wo.work_order_status_type_prfx = 'SF_WOSTAT'" +
		    " AND wods.dispatch_type_cd in " +
		    " ('NORMAL','CONTINUE','RECALL','SECOND','XFERTRADE','XFERVENDOR')" +
		    " AND wods.dispatch_type_prefix = 'SF_DISPTYP'" +
	        " AND wods.dispatch_by_type_cd in ('FAX','DISPEMAIL','EXTERNAL','DIRECT')" +
		    " AND wods.dispatch_by_type_prefix = 'SF_DISPBY'AND wo.contract_id = ?" +
	        " ORDER BY wods.dispatch_timestamp DESC";
    
    private static final String GET_CUSTOMER_DISPATCH_DATA_SQL = ""
            + "SELECT p.street_number, p.street_name, p.unit_type, p.unit_number, "
            + "t.type_description, cn.company_code, "
            + "p.street_direction, c.city_name, p.state, p.zip_code, cu.customer_id, "
            + "cu.title_type_code, cu.first_name, cu.middle_initial, cu.last_name, ia.internet_address, "
            + "cc.type_code as contract_customer_type, "
            + "da.dispatch_id, wods.dispatch_timestamp, "
            + "wl.covered_item_id, ci.item_description, ci.trade_code, cn.contract_id "
            + "FROM covered_items ci, work_order w, wo_line wl, wo_line_activity wla, "
            + "dispatch_activity_dtl da, work_order_dispatch_summary wods, property p, "
            + "cities c, contract cn, contract_customer cc, customer cu, "
            + "customer_internet_address cia, internet_address ia, types t "
            + "WHERE w.work_order_id = wl.work_order_id "
            + "AND t.type_code = NVL(p.unit_type,'ZZ') "
            + "AND t.type_prefix = 'UNITS' "
            + "AND w.property_id = p.property_id "
            + "AND p.city_code = c.city_code "
            + "AND ci.covered_item_id = wl.covered_item_id "
            + "AND wla.work_order_id = w.work_order_id "
            + "AND wla.wo_line_seq = wl.wo_line_seq "
            + "AND da.work_order_id = wla.work_order_id "
            + "AND da.wo_line_seq = wla.wo_line_seq "
            + "AND da.wo_line_acvtivity_seq = wla.wo_line_acvtivity_seq "
            + "AND wla.wo_line_activity_type_cd = 'DISPATCH' "
            + "AND wla.wo_line_activity_type_prefix = 'SF_WOACT' "
            + "AND w.work_order_id = wods.work_order_id "
            + "AND da.dispatch_id = wods.dispatch_id "
            + "AND w.contract_id = cn.contract_id "
            + "AND cn.contract_id = cc.contract_id "
            + "AND cc.customer_id = cu.customer_id "
            + "AND cc.type_code in ('BUY', 'SEL') "
            + "AND cu.customer_id = cia.customer_id "
            + "AND cia.internet_address_id = ia.internet_address_id "
            + "AND da.dispatch_id = ? ";
    
    private static final String GET_CUSTOMER_DISPATCH_DATA_SQL_COVERED_ITEM = ""
            + "AND wl.covered_item_id = ? ";
    
    private static final String GET_CUSTOMER_DISPATCH_DATA_SQL_ORDER_BY = ""
            + "ORDER BY cc.type_code";
    
    private static final String GET_CUSTOMER_PHONES_SQL = ""
            + "SELECT * "
            + "FROM customer_phones "
            + "WHERE customer_id = ?";
    
    private static final String IS_CASH_IN_LIEU_SQL = ""
            + "SELECT COUNT(*) AS count "
            + "FROM cil_summary cil, dispatch_activity_dtl dad, wo_line wol "
            + "WHERE cil.dispatch_id = ? "
            + "AND cil.cil_code = 'CILACCEPT' "
            + "AND cil.cil_prefix = 'SF_CIL' "
            + "AND cil.dispatch_id = dad.dispatch_id "
            + "AND dad.work_order_id = wol.work_order_id "
            + "AND dad.wo_line_seq = wol.wo_line_seq "
            + "AND wol.covered_item_id = ?";
    
    private static final String IS_CASH_IN_LIEU_BY_LINE_ID_SQL = ""
            + "SELECT COUNT(*) AS count "
            + "FROM cil_summary cil, dispatch_activity_dtl dad, wo_line wol "
            + "WHERE cil.dispatch_id = :dispatchId "
            + "AND cil.cil_code = 'CILACCEPT' "
            + "AND cil.cil_prefix = 'SF_CIL' "
            + "AND cil.dispatch_id = dad.dispatch_id "
            + "AND dad.work_order_id = wol.work_order_id "
            + "AND dad.wo_line_seq = wol.wo_line_seq "
            + "AND wol.wo_line_seq = :workOrderLineId";
    
    private static final String OPTED_FOR_REPLACEMENT_SQL = ""
            + "SELECT COUNT(*) AS count "
            + "FROM authorization a, authorization_line al, "
            + "report_grouping rg, dispatch_activity_dtl dad, wo_line wol "
            + "WHERE a.dispatch_id = ? "
            + "AND a.authorization_id = al.authorization_id "
            + "AND al.auth_type_code = rg.report_grouping_value "
            + "AND rg.type_prefix= 'AUTHOGRP1' "
            + "AND rg.type_code = '1REPLACE' "
            + "AND a.dispatch_id = dad.dispatch_id "
            + "AND dad.work_order_id = wol.work_order_id "
            + "AND dad.wo_line_seq = wol.wo_line_seq "
            + "AND wol.covered_item_id = ?";

	public WorkOrderDispatchSummaryDa findByPrimaryKey(long dispatchId) throws FinderException {
		
		LOGGER.info("ENTER: findByPrimaryKey()");

		WorkOrderDispatchSummaryDa workOrderDispatchSummaryDa = em.find(WorkOrderDispatchSummaryDa.class, dispatchId);

        if(workOrderDispatchSummaryDa == null){
            String msg = String.format("No entity found for dispatchId: %s",dispatchId);
            LOGGER.info(msg);
            workOrderDispatchSummaryDa = findByPrimaryKeyNoCache(dispatchId);
        }

        return workOrderDispatchSummaryDa;
	}

    public WorkOrderDispatchSummaryDa findByPrimaryKeyNoCache(long dispatchId) throws FinderException {

        LOGGER.info("ENTER: findByPrimaryKeyNoCache()");
        WorkOrderDispatchSummaryDa workOrderDispatchSummaryDa = null;
        try {
            workOrderDispatchSummaryDa = (WorkOrderDispatchSummaryDa) em
                    .createNamedQuery("WorkOrderDispatchSummaryDa.findByPrimaryKey")
                    .setParameter(1, dispatchId)
                    .setHint(QueryHints.CACHEABLE, false)
                    .getSingleResult();

        } catch (Exception exp){
            String msg = String.format("findByPrimaryKeyNoCache. No entity found for dispatchId: %s",dispatchId);
            LOGGER.info(msg,exp);
            throw new FinderException(msg);
        }

        return workOrderDispatchSummaryDa;
	}

	}
