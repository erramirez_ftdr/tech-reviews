package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the AGENT_INTERNET_ADDRESS database table.
 * 
 */
@Embeddable
public class AgentInternetAddressDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="AGENT_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long agentId;

	@Column(name="AGENT_TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String agentTypeCode;

	@Column(name="AGENT_TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String agentTypePrefix;

	@Column(name="INTERNET_ADDRESS_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long internetAddressId;

	public AgentInternetAddressDaPK() {
	}
	public long getAgentId() {
		return this.agentId;
	}
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}
	public String getAgentTypeCode() {
		return this.agentTypeCode;
	}
	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}
	public String getAgentTypePrefix() {
		return this.agentTypePrefix;
	}
	public void setAgentTypePrefix(String agentTypePrefix) {
		this.agentTypePrefix = agentTypePrefix;
	}
	public long getInternetAddressId() {
		return this.internetAddressId;
	}
	public void setInternetAddressId(long internetAddressId) {
		this.internetAddressId = internetAddressId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AgentInternetAddressDaPK)) {
			return false;
		}
		AgentInternetAddressDaPK castOther = (AgentInternetAddressDaPK)other;
		return 
			(this.agentId == castOther.agentId)
			&& this.agentTypeCode.equals(castOther.agentTypeCode)
			&& this.agentTypePrefix.equals(castOther.agentTypePrefix)
			&& (this.internetAddressId == castOther.internetAddressId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.agentId ^ (this.agentId >>> 32)));
		hash = hash * prime + this.agentTypeCode.hashCode();
		hash = hash * prime + this.agentTypePrefix.hashCode();
		hash = hash * prime + ((int) (this.internetAddressId ^ (this.internetAddressId >>> 32)));
		
		return hash;
	}
}