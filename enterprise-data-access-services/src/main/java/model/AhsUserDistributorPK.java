package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the AHS_USER_DISTRIBUTOR database table.
 * 
 */
@Embeddable
public class AhsUserDistributorPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="USER_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long userId;

	@Column(name="DISTRIBUTOR_ID", unique=true, nullable=false, precision=20)
	private long distributorId;

	@Column(name="DISTRIBUTOR_TYPE_CODE", unique=true, nullable=false, length=10)
	private String distributorTypeCode;

	@Column(name="DISTRIBUTOR_TYPE_PREFIX", unique=true, nullable=false, length=10)
	private String distributorTypePrefix;

	@Column(name="RELATIONSHIP_TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String relationshipTypeCode;

	@Column(name="RELATIONSHIP_TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String relationshipTypePrefix;

	public AhsUserDistributorPK() {
	}
	public long getUserId() {
		return this.userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getDistributorId() {
		return this.distributorId;
	}
	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}
	public String getDistributorTypeCode() {
		return this.distributorTypeCode;
	}
	public void setDistributorTypeCode(String distributorTypeCode) {
		this.distributorTypeCode = distributorTypeCode;
	}
	public String getDistributorTypePrefix() {
		return this.distributorTypePrefix;
	}
	public void setDistributorTypePrefix(String distributorTypePrefix) {
		this.distributorTypePrefix = distributorTypePrefix;
	}
	public String getRelationshipTypeCode() {
		return this.relationshipTypeCode;
	}
	public void setRelationshipTypeCode(String relationshipTypeCode) {
		this.relationshipTypeCode = relationshipTypeCode;
	}
	public String getRelationshipTypePrefix() {
		return this.relationshipTypePrefix;
	}
	public void setRelationshipTypePrefix(String relationshipTypePrefix) {
		this.relationshipTypePrefix = relationshipTypePrefix;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AhsUserDistributorPK)) {
			return false;
		}
		AhsUserDistributorPK castOther = (AhsUserDistributorPK)other;
		return 
			(this.userId == castOther.userId)
			&& (this.distributorId == castOther.distributorId)
			&& this.distributorTypeCode.equals(castOther.distributorTypeCode)
			&& this.distributorTypePrefix.equals(castOther.distributorTypePrefix)
			&& this.relationshipTypeCode.equals(castOther.relationshipTypeCode)
			&& this.relationshipTypePrefix.equals(castOther.relationshipTypePrefix);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.userId ^ (this.userId >>> 32)));
		hash = hash * prime + ((int) (this.distributorId ^ (this.distributorId >>> 32)));
		hash = hash * prime + this.distributorTypeCode.hashCode();
		hash = hash * prime + this.distributorTypePrefix.hashCode();
		hash = hash * prime + this.relationshipTypeCode.hashCode();
		hash = hash * prime + this.relationshipTypePrefix.hashCode();
		
		return hash;
	}
}