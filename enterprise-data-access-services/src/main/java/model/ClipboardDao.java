/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.ahslink.ejb.entity.EntityHelper;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

//import java.util.Hashtable;

/**
 *
 * @author mzafar
 */
@Stateless
public class ClipboardDao {
    
    private static final Logger LOGGER = Logger.getLogger(ClipboardDao.class);
    
    private static final String FIND_EXCEPTION_ZIP_CODES = "SELECT c.misc_text_1 state,c.misc_text_2 zip_code " +
                                                            "FROM clipboard c where c.group_code='TAXRATEEXP' " +
                                                            "ORDER BY state";
    
    public HashMap getExceptionStatesAndZipCodes() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet;
        HashMap exceptionSateAndZipCodes = new HashMap(600);
        
        try {            
             connection = EntityHelper.getDataSourceConnection();
LOGGER.debug("\n MZ. FIND_EXCEPTION_ZIP_CODES: " + FIND_EXCEPTION_ZIP_CODES);
             preparedStatement = connection.prepareStatement(FIND_EXCEPTION_ZIP_CODES);
      
             resultSet = preparedStatement.executeQuery();
LOGGER.debug("\n MZ. After executing query");             
             while (resultSet.next()) {

                String state = resultSet.getString(1);
                String zipCode = resultSet.getString(2);
                
                LOGGER.debug("\n MZ. About to add state: " + state + " and zip code:" + zipCode + " in exceptionSateAndZipCodes");
                        
                //Add the exception zip code and state in hashtable
                exceptionSateAndZipCodes.put(zipCode, state);
             }
        } catch (Exception ex) {
        	
        	LOGGER.error("Error occured while trying to get exception zip codes and states: " + ex.toString());
                //throw ex;

        } finally {

            try {
                EntityHelper.cleanupDataSource(connection, preparedStatement);
            } catch (Exception ex) {

            }
        }        
       
        return exceptionSateAndZipCodes;
    }
    
}
