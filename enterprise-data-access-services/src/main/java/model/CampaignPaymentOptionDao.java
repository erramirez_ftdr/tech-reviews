package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class CampaignPaymentOptionDao {

	@PersistenceContext(unitName=AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

	public Collection findByCampaignId(long campaignId) {
		return (Collection) em.createNamedQuery("CampaignPaymentOptionDa.findByCampaignId")
				.setParameter(1, campaignId)
				.getResultList();
	}
}
