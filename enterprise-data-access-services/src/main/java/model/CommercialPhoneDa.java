package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the COMMERCIAL_PHONE database table.
 * 
 */
@Entity
@Table(name="COMMERCIAL_PHONE")
@NamedQueries({
	@NamedQuery(name="CommercialPhoneDa.findAll", query="SELECT c FROM CommercialPhoneDa c"),
	@NamedQuery(name="CommercialPhoneDa.findByAddressId",
		query="SELECT c FROM CommercialPhoneDa c WHERE c.id.commercialPropertyId=?1")
})
public class CommercialPhoneDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CommercialPhoneDaPK id;

	@Column(length=10)
	private String extension;

	@Column(length=10)
	private String instructions;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", length=31)
	private String lastModifiedBy;

	public CommercialPhoneDa() {
	}

	public CommercialPhoneDaPK getId() {
		return this.id;
	}

	public void setId(CommercialPhoneDaPK id) {
		this.id = id;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getInstructions() {
		return this.instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

}