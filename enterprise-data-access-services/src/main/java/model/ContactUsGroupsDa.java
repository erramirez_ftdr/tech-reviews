package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the CONTACT_US_GROUPS database table.
 * 
 */
@Entity
@Table(name="CONTACT_US_GROUPS")
@NamedQueries({
	@NamedQuery(name="ContactUsGroupsDa.findAll", query="SELECT c FROM ContactUsGroupsDa c"),
	@NamedQuery(name="ContactUsGroupsDa.findByType",
		query="SELECT c FROM ContactUsGroupsDa c WHERE c.typeCode=?1"),
	@NamedQuery(name="ContactUsGroupsDa.findByPrimaryKey",
		query="SELECT c FROM ContactUsGroupsDa c WHERE c.contactUsTopicGroupId=?1")	
})
public class ContactUsGroupsDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONTACT_US_TOPIC_GROUP_ID", unique=true, nullable=false, precision=20)
	private long contactUsTopicGroupId;

	@Column(name="GROUP_DESCRIPTION", nullable=false, length=100)
	private String groupDescription;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", nullable=false, length=31)
	private String lastModifiedBy;

	@Column(name="TYPE_CODE", length=10)
	private String typeCode;

	@Column(name="TYPE_PREFIX", length=10)
	private String typePrefix;

	public ContactUsGroupsDa() {
	}

	public long getContactUsTopicGroupId() {
		return this.contactUsTopicGroupId;
	}

	public void setContactUsTopicGroupId(long contactUsTopicGroupId) {
		this.contactUsTopicGroupId = contactUsTopicGroupId;
	}

	public String getGroupDescription() {
		return this.groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getTypeCode() {
		return this.typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypePrefix() {
		return this.typePrefix;
	}

	public void setTypePrefix(String typePrefix) {
		this.typePrefix = typePrefix;
	}

}