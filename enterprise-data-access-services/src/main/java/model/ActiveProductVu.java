package model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the ACTIVE_PRODUCT_VU database table.
 * 
 */
@Entity
@Table(name="ACTIVE_PRODUCT_VU")
@NamedQuery(name="ActiveProductVu.findAll", query="SELECT a FROM ActiveProductVu a")
public class ActiveProductVu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="COMPANY_CODE", nullable=false, length=10)
	private String companyCode;

	@Id
	@Column(name="PRODUCT_ID", nullable=false, precision=20)
	private BigDecimal productId;

	@Column(name="PRODUCT_PLAN_ID", nullable=false, precision=20)
	private BigDecimal productPlanId;

	@Column(name="PRODUCT_VERSION_ID", nullable=false, precision=20)
	private BigDecimal productVersionId;

	@Column(name="SALES_CHANNEL", nullable=false, length=4)
	private String salesChannel;

	public ActiveProductVu() {
	}

	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public BigDecimal getProductId() {
		return this.productId;
	}

	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}

	public BigDecimal getProductPlanId() {
		return this.productPlanId;
	}

	public void setProductPlanId(BigDecimal productPlanId) {
		this.productPlanId = productPlanId;
	}

	public BigDecimal getProductVersionId() {
		return this.productVersionId;
	}

	public void setProductVersionId(BigDecimal productVersionId) {
		this.productVersionId = productVersionId;
	}

	public String getSalesChannel() {
		return this.salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

}