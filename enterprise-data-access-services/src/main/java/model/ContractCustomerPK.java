package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the CONTRACT_CUSTOMER database table.
 * 
 */
@Embeddable
public class ContractCustomerPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CONTRACT_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long contractId;

	@Column(name="CUSTOMER_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long customerId;

	@Column(name="TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String typeCode;

	@Column(name="TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String typePrefix;

	public ContractCustomerPK() {
	}
	public long getContractId() {
		return this.contractId;
	}
	public void setContractId(long contractId) {
		this.contractId = contractId;
	}
	public long getCustomerId() {
		return this.customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getTypeCode() {
		return this.typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTypePrefix() {
		return this.typePrefix;
	}
	public void setTypePrefix(String typePrefix) {
		this.typePrefix = typePrefix;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ContractCustomerPK)) {
			return false;
		}
		ContractCustomerPK castOther = (ContractCustomerPK)other;
		return 
			(this.contractId == castOther.contractId)
			&& (this.customerId == castOther.customerId)
			&& this.typeCode.equals(castOther.typeCode)
			&& this.typePrefix.equals(castOther.typePrefix);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.contractId ^ (this.contractId >>> 32)));
		hash = hash * prime + ((int) (this.customerId ^ (this.customerId >>> 32)));
		hash = hash * prime + this.typeCode.hashCode();
		hash = hash * prime + this.typePrefix.hashCode();
		
		return hash;
	}
}