package model;

import com.ahslink.config.AhsConstants;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class CommercialPhoneDao {

    @PersistenceContext(unitName= AhsConstants.ENTITY_MANAGER_UNIT_NAME)
    EntityManager em;

    public void create(
            long commercialPropertyId,
            String phoneNumber,
            String typeCode,
            String typePrefix,
            String instructions,
            String extension){
        CommercialPhoneDa newCommercialPhone = new CommercialPhoneDa();
        newCommercialPhone.setId(new CommercialPhoneDaPK());
        newCommercialPhone.getId().setCommercialPropertyId(commercialPropertyId);
        newCommercialPhone.getId().setPhoneNumber(phoneNumber);
        newCommercialPhone.getId().setTypeCode(typeCode);
        newCommercialPhone.getId().setTypePrefix(typePrefix);
        newCommercialPhone.setInstructions(instructions);
        newCommercialPhone.setExtension(extension);
        em.persist(newCommercialPhone);
    }

}
