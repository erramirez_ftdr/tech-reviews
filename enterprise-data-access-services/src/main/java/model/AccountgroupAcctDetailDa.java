package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the ACCOUNTGROUP_ACCT_DETAIL database table.
 * 
 */
@Entity
@Table(name="ACCOUNTGROUP_ACCT_DETAIL")
@NamedQuery(name="AccountgroupAcctDetailDa.findAll", query="SELECT a FROM AccountgroupAcctDetailDa a")
public class AccountgroupAcctDetailDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCOUNTGROUP_ACCT_DETAIL_ID", unique=true, nullable=false, precision=20)
	private long accountgroupAcctDetailId;

	@Column(name="ACCOUNTGROUP_ACCOUNT_ID", nullable=false, precision=20)
	private java.math.BigDecimal accountgroupAccountId;

	@Column(name="DETAIL_TYPE_CODE", nullable=false, length=10)
	private String detailTypeCode;

	@Column(name="DETAIL_TYPE_PREFIX", nullable=false, length=10)
	private String detailTypePrefix;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE", nullable=false)
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRATION_DATE", nullable=false)
	private Date expirationDate;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", nullable=false, length=31)
	private String lastModifiedBy;

	@Column(name="\"VALUE\"", nullable=false, length=500)
	private String value;

	public AccountgroupAcctDetailDa() {
	}

	public long getAccountgroupAcctDetailId() {
		return this.accountgroupAcctDetailId;
	}

	public void setAccountgroupAcctDetailId(long accountgroupAcctDetailId) {
		this.accountgroupAcctDetailId = accountgroupAcctDetailId;
	}

	public java.math.BigDecimal getAccountgroupAccountId() {
		return this.accountgroupAccountId;
	}

	public void setAccountgroupAccountId(java.math.BigDecimal accountgroupAccountId) {
		this.accountgroupAccountId = accountgroupAccountId;
	}

	public String getDetailTypeCode() {
		return this.detailTypeCode;
	}

	public void setDetailTypeCode(String detailTypeCode) {
		this.detailTypeCode = detailTypeCode;
	}

	public String getDetailTypePrefix() {
		return this.detailTypePrefix;
	}

	public void setDetailTypePrefix(String detailTypePrefix) {
		this.detailTypePrefix = detailTypePrefix;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}