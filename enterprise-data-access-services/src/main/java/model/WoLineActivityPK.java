package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the WO_LINE_ACTIVITY database table.
 */
@Embeddable
public class WoLineActivityPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="WORK_ORDER_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long workOrderId;

	@Column(name="WO_LINE_SEQ", insertable=false, updatable=false, unique=true, nullable=false, precision=6)
	private long woLineSeq;

	@Column(name="WO_LINE_ACVTIVITY_SEQ", unique=true, nullable=false, precision=6)
	private long woLineAcvtivitySeq;

	public WoLineActivityPK() {
	}
	
	public WoLineActivityPK(long workOrderId, long woLineSeq, long woLineActivitySeq){
		this.workOrderId = workOrderId;
		this.woLineSeq = woLineSeq;
		this.woLineAcvtivitySeq = woLineActivitySeq;
	}
	
	public long getWorkOrderId() {
		return this.workOrderId;
	}
	public void setWorkOrderId(long workOrderId) {
		this.workOrderId = workOrderId;
	}
	public long getWoLineSeq() {
		return this.woLineSeq;
	}
	public void setWoLineSeq(long woLineSeq) {
		this.woLineSeq = woLineSeq;
	}
	public long getWoLineAcvtivitySeq() {
		return this.woLineAcvtivitySeq;
	}
	public void setWoLineAcvtivitySeq(long woLineAcvtivitySeq) {
		this.woLineAcvtivitySeq = woLineAcvtivitySeq;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof WoLineActivityPK)) {
			return false;
		}
		WoLineActivityPK castOther = (WoLineActivityPK)other;
		return 
			(this.workOrderId == castOther.workOrderId)
			&& (this.woLineSeq == castOther.woLineSeq)
			&& (this.woLineAcvtivitySeq == castOther.woLineAcvtivitySeq);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.workOrderId ^ (this.workOrderId >>> 32)));
		hash = hash * prime + ((int) (this.woLineSeq ^ (this.woLineSeq >>> 32)));
		hash = hash * prime + ((int) (this.woLineAcvtivitySeq ^ (this.woLineAcvtivitySeq >>> 32)));
		
		return hash;
	}

	@Override
	public String toString() {
		return "WoLineActivityPK [workOrderId=" + workOrderId + ", woLineSeq="
				+ woLineSeq + ", woLineAcvtivitySeq=" + woLineAcvtivitySeq
				+ "]";
	}
}