package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the COVERED_ITEM_ATTR_TEMPLATE database table.
 * 
 */
@Embeddable
public class AttributeTemplateDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COVERED_ITEM_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long coveredItemId;

	@Column(name="COVERED_ITEM_ATTR_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long coveredItemAttrId;

	public AttributeTemplateDaPK() {
	}
	public AttributeTemplateDaPK(long coveredItemId, Long coveredItemAttrId) {
		this.coveredItemId = coveredItemId;
		this.coveredItemAttrId = coveredItemAttrId;
	}
	public long getCoveredItemId() {
		return this.coveredItemId;
	}
	public void setCoveredItemId(long coveredItemId) {
		this.coveredItemId = coveredItemId;
	}
	public long getCoveredItemAttrId() {
		return this.coveredItemAttrId;
	}
	public void setCoveredItemAttrId(long coveredItemAttrId) {
		this.coveredItemAttrId = coveredItemAttrId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AttributeTemplateDaPK)) {
			return false;
		}
		AttributeTemplateDaPK castOther = (AttributeTemplateDaPK)other;
		return 
			(this.coveredItemId == castOther.coveredItemId)
			&& (this.coveredItemAttrId == castOther.coveredItemAttrId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.coveredItemId ^ (this.coveredItemId >>> 32)));
		hash = hash * prime + ((int) (this.coveredItemAttrId ^ (this.coveredItemAttrId >>> 32)));
		
		return hash;
	}
}