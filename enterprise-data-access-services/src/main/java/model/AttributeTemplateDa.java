package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the COVERED_ITEM_ATTR_TEMPLATE database table.
 * 
 */
@Entity
@Table(name="COVERED_ITEM_ATTR_TEMPLATE")
@NamedQuery(name="AttributeTemplateDa.findAll", query="SELECT a FROM AttributeTemplateDa a")
public class AttributeTemplateDa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AttributeTemplateDaPK id;

	@Column(name="ECOMMERCE_ENABLED", length=10)
	private String ecommerceEnabled;

	@Column(length=1)
	private String feature;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", length=31)
	private String lastModifiedBy;

	@Column(name="REQUIRED_ATTRIBUTE", length=1)
	private String requiredAttribute;

	public AttributeTemplateDa() {
	}

	public AttributeTemplateDaPK getId() {
		return this.id;
	}

	public void setId(AttributeTemplateDaPK id) {
		this.id = id;
	}

	public String getEcommerceEnabled() {
		return this.ecommerceEnabled;
	}

	public void setEcommerceEnabled(String ecommerceEnabled) {
		this.ecommerceEnabled = ecommerceEnabled;
	}

	public String getFeature() {
		return this.feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getRequiredAttribute() {
		return this.requiredAttribute;
	}

	public void setRequiredAttribute(String requiredAttribute) {
		this.requiredAttribute = requiredAttribute;
	}

}