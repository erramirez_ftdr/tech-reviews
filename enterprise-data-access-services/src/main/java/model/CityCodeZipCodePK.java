package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the CITY_CODE_ZIP_CODE database table.
 * 
 */
@Embeddable
public class CityCodeZipCodePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ZIP_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String zipCode;

	@Column(name="CITY_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private long cityCode;

	public CityCodeZipCodePK() {
	}
	public String getZipCode() {
		return this.zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public long getCityCode() {
		return this.cityCode;
	}
	public void setCityCode(long cityCode) {
		this.cityCode = cityCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CityCodeZipCodePK)) {
			return false;
		}
		CityCodeZipCodePK castOther = (CityCodeZipCodePK)other;
		return 
			this.zipCode.equals(castOther.zipCode)
			&& (this.cityCode == castOther.cityCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.zipCode.hashCode();
		hash = hash * prime + ((int) (this.cityCode ^ (this.cityCode >>> 32)));
		
		return hash;
	}
	
	@Override
	public String toString() {
		return "CityCodeZipCodePK [zipCode=" + zipCode + ", cityCode="
				+ cityCode + "]";
	}
}