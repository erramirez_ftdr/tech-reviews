package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the AHS_USER_DETAILS database table.
 * 
 */
@Embeddable
public class AhsUserDetailsDaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="USER_ID", insertable=false, updatable=false, unique=true, nullable=false, precision=20)
	private long userId;

	@Column(name="TYPE_CODE", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String typeCode;

	@Column(name="TYPE_PREFIX", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String typePrefix;

	public AhsUserDetailsDaPK() {
	}
	public long getUserId() {
		return this.userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getTypeCode() {
		return this.typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTypePrefix() {
		return this.typePrefix;
	}
	public void setTypePrefix(String typePrefix) {
		this.typePrefix = typePrefix;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AhsUserDetailsDaPK)) {
			return false;
		}
		AhsUserDetailsDaPK castOther = (AhsUserDetailsDaPK)other;
		return 
			(this.userId == castOther.userId)
			&& this.typeCode.equals(castOther.typeCode)
			&& this.typePrefix.equals(castOther.typePrefix);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.userId ^ (this.userId >>> 32)));
		hash = hash * prime + this.typeCode.hashCode();
		hash = hash * prime + this.typePrefix.hashCode();
		
		return hash;
	}
}