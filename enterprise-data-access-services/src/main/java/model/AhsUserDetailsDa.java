package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the AHS_USER_DETAILS database table.
 * 
 */
@Entity
@Table(name="AHS_USER_DETAILS")
@NamedQuery(name="AhsUserDetailsDa.findAll", query="SELECT a FROM AhsUserDetailsDa a")
public class AhsUserDetailsDa implements Serializable {
	@Override
	public String toString() {
		return "AhsUserDetailsDa [id=" + id + ", effectiveDate="
				+ effectiveDate + ", expirationDate=" + expirationDate
				+ ", lastModified=" + lastModified + ", lastModifiedBy="
				+ lastModifiedBy + ", typeValue=" + typeValue + "]";
	}

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AhsUserDetailsDaPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE", nullable=false)
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED", nullable=false)
	private Date lastModified;

	@Column(name="LAST_MODIFIED_BY", length=31)
	private String lastModifiedBy;

	@Column(name="TYPE_VALUE", length=50)
	private String typeValue;

	public AhsUserDetailsDa() {
	}

	public AhsUserDetailsDaPK getId() {
		return this.id;
	}

	public void setId(AhsUserDetailsDaPK id) {
		this.id = id;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getTypeValue() {
		return this.typeValue;
	}

	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}

}